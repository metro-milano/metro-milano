package com.sirowain.mm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Scroller;
import android.widget.ZoomButtonsController;
import android.widget.ZoomButtonsController.OnZoomListener;

public class Scroll extends View implements OnGestureListener, OnZoomListener {

	
	private int WIDTH = 1707;
	private int HEIGHT = 1200;
	private static final float MAX_ZOOM = 1;

	private float mX;
	private float mY;
	private Scroller mScroller;
	private GestureDetector mGestureDetector;
	private float mScale;
	private ZoomButtonsController mZoomController;
	private static Bitmap map;
	private static boolean isHigh;
	
	public Scroll(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (getResources().getDisplayMetrics().densityDpi == DisplayMetrics.DENSITY_HIGH)
			isHigh = true;
		//options.inSampleSize=2;
		mScroller = new Scroller(context);
		mGestureDetector = new GestureDetector(this);
		if (isHigh)
			mScale = (float) 0.8;
		else
			mScale = (float) 0.5;
		mZoomController = new ZoomButtonsController(this);
		mZoomController.setAutoDismissed(true);
		mZoomController.setOnZoomListener(this);
		mZoomController.setZoomSpeed(25);
		mZoomController.setZoomInEnabled(true);

		makeContent();
		if (isHigh)
			mScroller.startScroll(0, 0, -(int)(WIDTH*mScale/3.5), -HEIGHT*(int)mScale/2, 1);
		else
			mScroller.startScroll(0, 0, -(int)(WIDTH*mScale/2.5), -HEIGHT*(int)mScale/2, 1);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		mZoomController.setVisible(false);
		map.recycle();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return mGestureDetector.onTouchEvent(event);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		canvas.save();
		if (map.isRecycled()) {
			refreshBitmap();
		}
		if (mScroller.computeScrollOffset()) {
			mX = mScroller.getCurrX();
			mY = mScroller.getCurrY();
			invalidate();
		}
		canvas.translate(mX * mScale, mY * mScale);
		
		// change zoomTopLeft to false for "center" zooming
		boolean zoomTopLeft = true;
		if (zoomTopLeft) {
			canvas.scale(mScale, mScale);
			//canvas.rotate(mRotation);
		} else {
			canvas.scale(mScale, mScale, this.getMeasuredWidth() / 2, this.getMeasuredHeight() / 2);
			//canvas.rotate(mRotation);
		}
		Paint paint = new Paint();
		paint.setFilterBitmap(true);
		paint.setDither(false);
		paint.setAntiAlias(false);
		canvas.drawBitmap(map, 0, 0, paint);
		canvas.restore();
	}

	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		int minX = (int) (getWidth() / mScale - WIDTH);
		int minY = (int) (getHeight() / mScale - HEIGHT);
		mScroller.fling((int) mX, (int) mY, (int) velocityX, (int) velocityY, minX, 0, minY, 0);
		invalidate();
		return true;
	}

	public void onLongPress(MotionEvent e) {
		
	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		mX -= distanceX / mScale;
		mY -= distanceY / mScale;
		mX = Math.max(getWidth() / mScale - WIDTH, Math.min(0, mX));
		mY = Math.max(getHeight() / mScale - HEIGHT, Math.min(0, mY));
		invalidate();
		return true;
	}

	public void onShowPress(MotionEvent e) {
	}
	
	public boolean onDown(MotionEvent e) {
		mScroller.abortAnimation();
		return true;	
	}

	public boolean onSingleTapUp(MotionEvent e) {
		if (mZoomController.isVisible())
			mZoomController.setVisible(false);
		else 
			mZoomController.setVisible(true);
		return true;	
	}

	public void onVisibilityChanged(boolean visible) {
		invalidate();
	}

	public Bitmap getBitmap(){
		return map;
	}
	
	public void onZoom(boolean zoomIn) {
		mScale += zoomIn? 0.1 : -0.1;
		mScale = (float) Math.min(MAX_ZOOM, Math.max(0.3, mScale));
		mX = Math.max(getWidth() / mScale - WIDTH, Math.min(0, mX));
		mY = Math.max(getHeight() / mScale - HEIGHT, Math.min(0, mY));
		invalidate();
		
		mZoomController.setZoomInEnabled(mScale != MAX_ZOOM);
		mZoomController.setZoomOutEnabled(mScale > (float)0.3);
	}

	private void makeContent() {
		map = BitmapFactory.decodeResource(getResources(), R.drawable.metro_s);
		HEIGHT = map.getHeight();
		WIDTH = map.getWidth();
	}
	
	private Bitmap refreshBitmap() {
		map = BitmapFactory.decodeResource(getResources(), R.drawable.metro_s);
		return map;
	}
}