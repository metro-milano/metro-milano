/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.sirowain.mm.metro.TabMetro;
import com.sirowain.mm.passante.PassanteMain;
import com.sirowain.mm.route.BrowseAll;
import com.sirowain.mm.route.Route;

public class MilanMetro extends Activity {
	private static final int ROUTE_ID = 0;
	private Intent mMetroMap;
	private Intent mPassanteMap;
	private Intent mRoutePlanner;
	private Intent mAllStations;
	private Scroll mScrollView;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		/** Called when the activity is first created. */
		super.onCreate(savedInstanceState);
		/** No title bar */
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		/** Sets contens */
		setContentView(R.layout.metro);
		
		mRoutePlanner = new Intent(this, Route.class);
		mAllStations = new Intent(this, BrowseAll.class);
		mMetroMap = new Intent(this, TabMetro.class);
		
		/**
		 * Defines the layout
		 */
		Button mBtnMetro = (Button)findViewById(R.id.ButtonMetro);
		mBtnMetro.setText(R.string.metro);
		mBtnMetro.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(mMetroMap);	
			}
		});
		
		mPassanteMap = new Intent(this, PassanteMain.class);
		Button mBtnPassante = (Button)findViewById(R.id.ButtonPassante);
		mBtnPassante.setText(R.string.passante);
		mBtnPassante.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(mPassanteMap);	
			}
		});
		
		LinearLayout mMainLayout = (LinearLayout)findViewById(R.id.LinearLayout01);
		mMainLayout.removeViewAt(1);
		
		/** Scroll object for main map widget */
		mScrollView = new Scroll(getApplicationContext(), null);
		mMainLayout.addView(mScrollView, 1, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 10));
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		/** recycle bitmap on close */
		mScrollView.getBitmap().recycle();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, ROUTE_ID, 0, R.string.search);
		menu.add(0, 1, 0, R.string.all_stations);
		MenuItem provaButton2 = menu.getItem(1);
		provaButton2.setIcon(android.R.drawable.ic_menu_sort_alphabetically);
		provaButton2.setOnMenuItemClickListener(resetListener);
		MenuItem provaButton = menu.getItem(0);
		provaButton.setIcon(android.R.drawable.ic_search_category_default);
		provaButton.setOnMenuItemClickListener(resetListener);
		return super.onCreateOptionsMenu(menu);
	}

	OnMenuItemClickListener resetListener = new OnMenuItemClickListener() {
		public boolean onMenuItemClick(MenuItem item) {
			if (item.getItemId()==ROUTE_ID) {
				startActivity(mRoutePlanner);
			} else {
				startActivity(mAllStations);
			}
			return true;
		}
	};

}