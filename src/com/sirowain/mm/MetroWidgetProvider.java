/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.sirowain.mm.data.MetroData;
import com.sirowain.mm.metro.TabMetro;

public class MetroWidgetProvider extends AppWidgetProvider {

	private Intent mIntentM1;
	private Intent mIntentM2;
	private Intent mIntentM3;
	private RemoteViews mRemoteViews;
	
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		final int N = appWidgetIds.length;
		/**
		 * Perform this loop procedure for each App Widget
		 * that belongs to this provider
		 */
		for (int i = 0; i < N; i++) {
			int appWidgetId = appWidgetIds[i];

			/** Create an Intent to launch ExampleActivity */
			mRemoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget_metro);
			
			/** Red Line Button */
			mIntentM1 = new Intent(context, TabMetro.class);
			mIntentM1.putExtra(MetroData.APP_PACKAGE+".defaultTab", 0);
			PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
					mIntentM1, PendingIntent.FLAG_UPDATE_CURRENT);
			mRemoteViews.setOnClickPendingIntent(R.id.W_M1, pendingIntent);
			
			/** Green Line Button */
			mIntentM2 = new Intent(context, TabMetro.class);
			mIntentM2.putExtra(MetroData.APP_PACKAGE+".defaultTab", 1);
			pendingIntent = PendingIntent.getActivity(context, 1,
					mIntentM2, PendingIntent.FLAG_UPDATE_CURRENT);
			mRemoteViews.setOnClickPendingIntent(R.id.W_M2, pendingIntent);
			
			/** Yellow Line Button */
			mIntentM3 = new Intent(context, TabMetro.class);
			mIntentM3.putExtra(MetroData.APP_PACKAGE+".defaultTab", 2);
			pendingIntent = PendingIntent.getActivity(context, 2,
					mIntentM3, PendingIntent.FLAG_UPDATE_CURRENT);
			mRemoteViews.setOnClickPendingIntent(R.id.W_M3, pendingIntent);

			appWidgetManager.updateAppWidget(appWidgetId, mRemoteViews);
		}
	}
}