package com.sirowain.mm.data;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MetroUtils {

	/**
	 * Defines text style for search result
	 * 
	 * @return
	 */
	public static void setSearchStyle(SpannableString span) {
//		span.setSpan(new BackgroundColorSpan(Color.RED), 0, span.length(), 0);
//		span.setSpan(new UnderlineSpan(), 0, span.length(), 0);
		span.setSpan(new ForegroundColorSpan(Color.RED), 0, span.length(), 0);
	}

	public static class ViewHolder {
		public ImageView image1;
		public TextView text1;
		public LinearLayout layoutl;
		public LinearLayout layoutr;
		public ImageView imagel;
		public TextView textl;
		public ImageView imager;
		public TextView textr;
	}

	public static void selectSearchMetroD(ViewHolder holder, String namel,
			String namer, String searchLine) {

		if (namel != null && namer != null) {
			if (namel.equals(searchLine)) {
				SpannableString underlineString = new SpannableString(holder.textl.getText());
				setSearchStyle(underlineString);
				holder.textl.setText(underlineString);
			}
			if (namer.equals(searchLine)) {
				SpannableString underlineString = new SpannableString(holder.textr.getText());
				setSearchStyle(underlineString);
				holder.textr.setText(underlineString);
			}
		} else if (namer != null) {
			if (namer.equals(searchLine)) {
				SpannableString underlineString = new SpannableString(holder.textr.getText());
				setSearchStyle(underlineString);
				holder.textr.setText(underlineString);
			}
		} else if (namel != null) {
			if (namel.equals(searchLine)) {
				SpannableString underlineString = new SpannableString(holder.text1.getText());
				setSearchStyle(underlineString);
				holder.text1.setText(underlineString);
			}
		}
	}
}
