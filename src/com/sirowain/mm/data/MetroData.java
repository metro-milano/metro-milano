/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm.data;

import java.util.ArrayList;

public class MetroData {
	
	/**
	 * Set to 'true' to enable clickable lists
	 */
	public static boolean ENABLE_LIST = false;
	public static boolean ENABLE_ADS = true;
	
	/**
	 * Sets the padding of line elements
	 */
	public static int LIST_L_PAD = 5;
	public static int LIST_R_PAD = 0;
	public static int LIST_T_PAD = 0;
	public static int LIST_B_PAD = 0;
	
	/**
	 * Defines the application package
	 */
	public static String APP_PACKAGE = "com.sirowain.mm";
	
	/*
	 * Returns all stations
	 */
	public static ArrayList<ListElement> getAll() {
		final ArrayList<ListElement> all_lines = new ArrayList<ListElement>();
		all_lines.addAll(getRedLineBranch());
		all_lines.addAll(getRedLineMain());
		all_lines.addAll(getGreenLineMain());
		all_lines.addAll(getGreenLineBranch());
		all_lines.addAll(getGreenLineNew2011());
		all_lines.addAll(getYellowLineMain());
		all_lines.addAll(SData.getS1());
		all_lines.addAll(SData.getS2());
		all_lines.addAll(SData.getS3());
		all_lines.addAll(SData.getS4());
		all_lines.addAll(SData.getS5());
		all_lines.addAll(SData.getS6());
		all_lines.addAll(SData.getS8());
		all_lines.addAll(SData.getS9());
		all_lines.addAll(SData.getS11());
		all_lines.addAll(SData.getS13());
		return all_lines;
	}
	
	/*
	 * Returns all enums for all lines
	 */
	public static ArrayList<MetroEnum> getAllEnums() {
		final ArrayList<MetroEnum> all_enums = new ArrayList<MetroEnum>();
		all_enums.add(MetroEnum.M1);
		all_enums.add(MetroEnum.M2);
		all_enums.add(MetroEnum.M3);
		all_enums.add(MetroEnum.S1);
		all_enums.add(MetroEnum.S2);
		all_enums.add(MetroEnum.S3);
		all_enums.add(MetroEnum.S4);
		all_enums.add(MetroEnum.S5);
		all_enums.add(MetroEnum.S6);
		all_enums.add(MetroEnum.S7);
		all_enums.add(MetroEnum.S8);
		all_enums.add(MetroEnum.S9);
		all_enums.add(MetroEnum.S11);
		all_enums.add(MetroEnum.S13);
		return all_enums;
	}

	/**
	 * Defines the RED metro line
	 */
	public static ArrayList<MetroElement> getRedLineMain() {
		final ArrayList<MetroElement> line_main = new ArrayList<MetroElement>();
		line_main.add(new MetroElement(MetroEnum.M1, "Sesto (I Maggio)", MetroEnum.ELEMENT_FIRST));
		line_main.add(new MetroElement(MetroEnum.M1, "Sesto Rond�",true));
		line_main.add(new MetroElement(MetroEnum.M1, "Sesto Marelli"));
		line_main.add(new MetroElement(MetroEnum.M1, "Villa San Giovanni"));
		line_main.add(new MetroElement(MetroEnum.M1, "Precotto"));
		line_main.add(new MetroElement(MetroEnum.M1, "Gorla"));
		line_main.add(new MetroElement(MetroEnum.M1, "Turro"));
		line_main.add(new MetroElement(MetroEnum.M1, "Rovereto"));
		line_main.add(new MetroElement(MetroEnum.M1, "Pasteur"));
		line_main.add(new MetroElement(MetroEnum.M1, "Loreto", MetroEnum.ELEMENT_GREEN));
		line_main.add(new MetroElement(MetroEnum.M1, "Lima"));
		line_main.add(new MetroElement(MetroEnum.M1, "P.ta Venezia"));
		line_main.add(new MetroElement(MetroEnum.M1, "Palestro"));
		line_main.add(new MetroElement(MetroEnum.M1, "S. Babila"));
		line_main.add(new MetroElement(MetroEnum.M1, "Duomo", MetroEnum.ELEMENT_YELLOW));
		line_main.add(new MetroElement(MetroEnum.M1, "Cordusio"));
		line_main.add(new MetroElement(MetroEnum.M1, "Cairoli"));
		line_main.add(new MetroElement(MetroEnum.M1, "Cadorna", MetroEnum.ELEMENT_GREEN));
		line_main.add(new MetroElement(MetroEnum.M1, "Conciliazione"));
		line_main.add(new MetroElement(MetroEnum.M1, "Pagano"));
		/** start branch */
		line_main.add(new MetroElement(MetroEnum.ELEMENT_RACC, "", MetroEnum.ELEMENT_RACC));
		line_main.add(new MetroElement(MetroEnum.M1, "Buonarroti"));
		line_main.add(new MetroElement(MetroEnum.M1, "Amendola"));
		line_main.add(new MetroElement(MetroEnum.M1, "Lotto"));
		line_main.add(new MetroElement(MetroEnum.M1, "QT8"));
		line_main.add(new MetroElement(MetroEnum.M1, "Lampugnano"));
		line_main.add(new MetroElement(MetroEnum.M1, "Uruguay"));
		line_main.add(new MetroElement(MetroEnum.M1, "Bonola"));
		line_main.add(new MetroElement(MetroEnum.M1, "S. Leonardo"));
		line_main.add(new MetroElement(MetroEnum.M1, "Molino-Dorino"));
		line_main.add(new MetroElement(MetroEnum.M1, "Pero",true));
		line_main.add(new MetroElement(MetroEnum.M1, "Rho-Fiera Milano", MetroEnum.ELEMENT_LAST));
		return line_main;
	}
	/**
	 * Defines the RED metro line's secondary branch
	 */
	public static ArrayList<MetroElement> getRedLineBranch() {
		final ArrayList<MetroElement> line_branch = new ArrayList<MetroElement>();
		line_branch.add(new MetroElement(MetroEnum.M1, "Wagner"));
		line_branch.add(new MetroElement(MetroEnum.M1, "De Angeli"));
		line_branch.add(new MetroElement(MetroEnum.M1, "Gambara"));
		line_branch.add(new MetroElement(MetroEnum.M1, "Bande Nere"));
		line_branch.add(new MetroElement(MetroEnum.M1, "Primaticcio"));
		line_branch.add(new MetroElement(MetroEnum.M1, "Inganni"));
		line_branch.add(new MetroElement(MetroEnum.M1, "Bisceglie", MetroEnum.ELEMENT_LAST));
		return line_branch;
	}
	
	/**
	 * Defines the YELLOW metro line	
	 */
	public static ArrayList<MetroElement> getYellowLineMain() {
		final ArrayList<MetroElement> line_main = new ArrayList<MetroElement>();
		line_main.add(new MetroElement(MetroEnum.M3, "Comasina", MetroEnum.ELEMENT_FIRST));
		line_main.add(new MetroElement(MetroEnum.M3, "Affori FN"));
		line_main.add(new MetroElement(MetroEnum.M3, "Affori Centro"));
		line_main.add(new MetroElement(MetroEnum.M3, "Dergano"));
		line_main.add(new MetroElement(MetroEnum.M3, "Maciachini"));
		line_main.add(new MetroElement(MetroEnum.M3, "Zara"));
		line_main.add(new MetroElement(MetroEnum.M3, "Sondrio"));
		line_main.add(new MetroElement(MetroEnum.M3, "Centrale", MetroEnum.ELEMENT_GREEN));
		line_main.add(new MetroElement(MetroEnum.M3, "Repubblica"));
		line_main.add(new MetroElement(MetroEnum.M3, "Turati"));
		line_main.add(new MetroElement(MetroEnum.M3, "Montenapoleone"));
		line_main.add(new MetroElement(MetroEnum.M3, "Duomo", MetroEnum.ELEMENT_RED));
		line_main.add(new MetroElement(MetroEnum.M3, "Missori"));
		line_main.add(new MetroElement(MetroEnum.M3, "Crocetta"));
		line_main.add(new MetroElement(MetroEnum.M3, "P.ta Romana"));
		line_main.add(new MetroElement(MetroEnum.M3, "Lodi Tibb"));
		line_main.add(new MetroElement(MetroEnum.M3, "Brenta"));
		line_main.add(new MetroElement(MetroEnum.M3, "Corvetto"));
		line_main.add(new MetroElement(MetroEnum.M3, "Porto di Mare"));
		line_main.add(new MetroElement(MetroEnum.M3, "Rogoredo"));
		line_main.add(new MetroElement(MetroEnum.M3, "S. Donato", MetroEnum.ELEMENT_LAST));
		return line_main;
	}
	
	/**
	 * Defines the GREEN metro line
	 */
	public static ArrayList<MetroElement> getGreenLineMain() {
		final ArrayList<MetroElement> line_main = new ArrayList<MetroElement>();
		line_main.add(new MetroElement(MetroEnum.M2, "Gessate", MetroEnum.ELEMENT_FIRST));
		line_main.add(new MetroElement(MetroEnum.M2, "Cascina Antonietta"));
		line_main.add(new MetroElement(MetroEnum.M2, "Gorgonzola"));
		line_main.add(new MetroElement(MetroEnum.M2, "Villa Pompea"));
		line_main.add(new MetroElement(MetroEnum.M2, "Bussero"));
		line_main.add(new MetroElement(MetroEnum.M2, "Cascina de'Pecchi"));
		line_main.add(new MetroElement(MetroEnum.M2, "Villa Fiorita"));
		/** start branch */
		line_main.add(new MetroElement(MetroEnum.M2, "Cernusco S.N."));
		line_main.add(new MetroElement(MetroEnum.M2, "Cascina Burrona"));
		line_main.add(new MetroElement(MetroEnum.M2, "Vimodrone"));
		/** end branch */
		line_main.add(new MetroElement(MetroEnum.ELEMENT_RACC, "",MetroEnum.ELEMENT_RACC));
		line_main.add(new MetroElement(MetroEnum.M2, "Cascina Gobba"));
		line_main.add(new MetroElement(MetroEnum.M2, "Grescenzago"));
		line_main.add(new MetroElement(MetroEnum.M2, "Cimiano"));
		line_main.add(new MetroElement(MetroEnum.M2, "Udine"));
		line_main.add(new MetroElement(MetroEnum.M2, "Lambrate FS"));
		line_main.add(new MetroElement(MetroEnum.M2, "Piola"));
		line_main.add(new MetroElement(MetroEnum.M2, "Loreto", MetroEnum.ELEMENT_RED));
		line_main.add(new MetroElement(MetroEnum.M2, "Caiazzo"));
		line_main.add(new MetroElement(MetroEnum.M2, "Centrale", MetroEnum.ELEMENT_YELLOW));
		line_main.add(new MetroElement(MetroEnum.M2, "Gioia"));
		line_main.add(new MetroElement(MetroEnum.M2, "Garibaldi"));
		line_main.add(new MetroElement(MetroEnum.M2, "Moscova"));
		line_main.add(new MetroElement(MetroEnum.M2, "Lanza"));
		line_main.add(new MetroElement(MetroEnum.M2, "Cadorna",MetroEnum.ELEMENT_RED));
		line_main.add(new MetroElement(MetroEnum.M2, "S. Ambrogio"));
		line_main.add(new MetroElement(MetroEnum.M2, "S. Agostino"));
		line_main.add(new MetroElement(MetroEnum.M2, "P.ta Genova FS"));
		line_main.add(new MetroElement(MetroEnum.M2, "Romolo"));
		line_main.add(new MetroElement(MetroEnum.M2, "Famagosta"));
		/** start branch 2011 */
		line_main.add(new MetroElement(MetroEnum.ELEMENT_RACC, "",MetroEnum.ELEMENT_RACC));
		line_main.add(new MetroElement(MetroEnum.M2, "Abbiategrasso", MetroEnum.ELEMENT_LAST));
		return line_main;
	}
	/**
	 * Defines the GREEN metro line's secondary branch 
	 */
	public static ArrayList<MetroElement> getGreenLineBranch() {
		final ArrayList<MetroElement> line_branch = new ArrayList<MetroElement>();
		line_branch.add(new MetroElement(MetroEnum.M2, "Cologno Nord",MetroEnum.ELEMENT_FIRST));
		line_branch.add(new MetroElement(MetroEnum.M2, "Cologno Centro"));
		line_branch.add(new MetroElement(MetroEnum.M2, "Cologno Sud"));
		return line_branch;
	}
	
	/**
	 * Defines the GREEN metro line's 2011 new branch 
	 */
	public static ArrayList<MetroElement> getGreenLineNew2011() {
		final ArrayList<MetroElement> line_branch = new ArrayList<MetroElement>();
		line_branch.add(new MetroElement(MetroEnum.M2, "Assago Milanofiori Nord"));
		line_branch.add(new MetroElement(MetroEnum.M2, "Assago Milanofiori Forum", MetroEnum.ELEMENT_LAST));
		return line_branch;
	}
}
