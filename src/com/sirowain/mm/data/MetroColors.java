/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm.data;

public interface MetroColors {

	/**
	 * The colors of metro lines
	 */
	public static final String rosso = "#ff0000";
	public static final String giallo = "#ffb600";
	public static final String verde= "#08ca00";
	
	/**
	 * The colors of suburban railway lines
	 */
	public static final String s1 = "#c7282a";
	public static final String s2 = "#33b540";
	public static final String s3 = "#a04077";
	public static final String s4 = "#9ad53b";
	public static final String s5 = "#ef641c";
	public static final String s6 = "#eebc23";
	public static final String s8 = "#f799d1";
	public static final String s9 = "#8c2b99";
	public static final String s11 = "#caaadb";
	public static final String s13 = "#784421";
}
