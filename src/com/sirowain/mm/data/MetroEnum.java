/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm.data;

public enum MetroEnum {
	M1 ("M1", "Linea Rossa"),
	M2 ("M2", "Linea Verde"),
	M3 ("M3", "Linea Gialla"),
	S1 ("S1", "Saronno <> Rogoredo <> Lodi"),
	S2 ("S2", "Rogoredo <> Seveso <> Mariano"),
	S3 ("S3", "Cadorna <> Saronno"),
	S4 ("S4", "Cadorna <> Camnago / (Meda)"),
	S5 ("S5", "Varese <> Treviglio"),
	S6 ("S6", "Novara <> Pioltello <> Treviglio"),
	S7 ("S7", "Besana - Milano P.ta Garibaldi"),
	S8 ("S8", "Porta Garibaldi <> Lecco"),
	S9 ("S9", "Albairate <> Seregno"),
	//S10 ("S10", "Rogoredo <> Bovisa"),
	S11 ("S11", "Porta Garibaldi <> Chiasso"),
	S13 ("S13", "Milano Bovisa <> Pavia"),
	ELEMENT_FIRST,
	ELEMENT_LAST,
	ELEMENT_NORM,
	ELEMENT_YELLOW,
	ELEMENT_GREEN,
	ELEMENT_RED,
	ELEMENT_RACC,
	R,
	Mxp,
	Lin;
	
	private String name;
	private String descr;
	
	MetroEnum(String name, String descr){
		this.setName(name);
		this.setDescr(descr);
	}
	MetroEnum(){
		
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}
	
	public void setDescr(String descr) {
		this.descr = descr;
	}
}
