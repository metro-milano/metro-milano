/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SData {
	
	/**
	 * set to 'true' to enable clickable lists
	 */
	public static boolean ENABLE_LIST = false;
	
	/**
	 * Defines the lines names to show in main menu
	 */
	static Map<String, String> lines_names = new HashMap<String, String>();
	static {
		lines_names.put("S1", "Saronno <> Rogoredo <> Lodi");
		lines_names.put("S2", "Rogoredo <> Seveso <> Mariano");
		lines_names.put("S3", "Cadorna <> Saronno");
		lines_names.put("S4", "Cadorna <> Camnago / (Meda)");
		lines_names.put("S5", "Varese <> Treviglio");
		lines_names.put("S6", "Novara <> Pioltello <> Treviglio");
		lines_names.put("S8", "Porta Garibaldi <> Lecco");
		lines_names.put("S9", "Albairate <> Seregno");
		lines_names.put("S10", "SOPPRESSA");
		lines_names.put("S11", "Porta Garibaldi <> Chiasso");
		lines_names.put("S13", "Milano Bovisa <> Pavia");
	}
	
	public static String getName(String name) {
		return lines_names.get(name);
	}
	
	public static Map<String,String> getNomi() {
		return lines_names;
	}

	/**
	 * Returns line S1 elements
	 */
	public static ArrayList<SElement> getS1() {
		final ArrayList<SElement> line_elements = new ArrayList<SElement>();
		line_elements.add(new SElement(MetroEnum.S1, "Saronno", MetroEnum.S3,MetroEnum.R,MetroEnum.Mxp,null,null,null));
		line_elements.add(new SElement(MetroEnum.S1, "Saronno Sud"));
		line_elements.add(new SElement(MetroEnum.S1, "Caronno P."));
		line_elements.add(new SElement(MetroEnum.S1, "Cesate"));
		line_elements.add(new SElement(MetroEnum.S1, "Garbagnate M."));
		line_elements.add(new SElement(MetroEnum.S1, "Garbagnate P.Groane"));
		line_elements.add(new SElement(MetroEnum.S1, "Bollate Nord"));
		line_elements.add(new SElement(MetroEnum.S1, "Bollate Centro"));
		line_elements.add(new SElement(MetroEnum.S1, "Novate M."));
		line_elements.add(new SElement(MetroEnum.S1, "Quarto Oggiaro"));
		line_elements.add(new SElement(MetroEnum.S1, "Bovisa",MetroEnum.S2,MetroEnum.S3,MetroEnum.S4,MetroEnum.S13,MetroEnum.R,MetroEnum.Mxp));
		line_elements.add(new SElement(MetroEnum.S1, "Lancetti",MetroEnum.S5,MetroEnum.S6,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S1, "P.ta Garibaldi",MetroEnum.M2,MetroEnum.S8,MetroEnum.S11,MetroEnum.R,null,null));
		line_elements.add(new SElement(MetroEnum.S1, "Repubblica",MetroEnum.M3,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S1, "P.ta Venezia",MetroEnum.M1,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S1, "Dateo",MetroEnum.Lin,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S1, "P.ta Vittoria",MetroEnum.S5,MetroEnum.S6,MetroEnum.Lin,null,null,null));
		line_elements.add(new SElement(MetroEnum.S1, "Rogoredo",MetroEnum.M3,MetroEnum.S2,MetroEnum.S13,MetroEnum.R,null,null));
		line_elements.add(new SElement(MetroEnum.S1, "S. Donato (FS)"));
		line_elements.add(new SElement(MetroEnum.S1, "Borgolombardo"));
		line_elements.add(new SElement(MetroEnum.S1, "S. Giuliano M."));
		line_elements.add(new SElement(MetroEnum.S1, "Melegnano"));
		line_elements.add(new SElement(MetroEnum.S1, "S. Zenone"));
		line_elements.add(new SElement(MetroEnum.S1, "Tavazzano"));
		line_elements.add(new SElement(MetroEnum.S1, "Lodi",MetroEnum.R,null,null,null,null,null));
		return line_elements;
	}
	
	/**
	 * Returns line S2 elements
	 */
	public static ArrayList<SElement> getS2() {
		final ArrayList<SElement> line_elements = new ArrayList<SElement>();
		line_elements.add(new SElement(MetroEnum.S2, "Mariano C.",MetroEnum.R,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S2, "Cabiate"));
		line_elements.add(new SElement(MetroEnum.S2, "Meda"));
		line_elements.add(new SElement(MetroEnum.S2, "Seveso",MetroEnum.S4,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S2, "Cesano M."));
		line_elements.add(new SElement(MetroEnum.S2, "Bovisio M."));
		line_elements.add(new SElement(MetroEnum.S2, "Varedo"));
		line_elements.add(new SElement(MetroEnum.S2, "Palazzolo M."));
		line_elements.add(new SElement(MetroEnum.S2, "Paderno D."));
		line_elements.add(new SElement(MetroEnum.S2, "Cusano M."));
		line_elements.add(new SElement(MetroEnum.S2, "Cormano"));
		line_elements.add(new SElement(MetroEnum.S2, "Bruzzano"));
		line_elements.add(new SElement(MetroEnum.S2, "Affori"));
		line_elements.add(new SElement(MetroEnum.S2, "Bovisa",MetroEnum.S1,MetroEnum.S3,MetroEnum.S4,MetroEnum.S13,MetroEnum.R,MetroEnum.Mxp));
		line_elements.add(new SElement(MetroEnum.S2, "Lancetti",MetroEnum.S5,MetroEnum.S6,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S2, "P.ta Garibaldi",MetroEnum.M2,MetroEnum.S8,MetroEnum.S11,MetroEnum.R,null,null));
		line_elements.add(new SElement(MetroEnum.S2, "Repubblica",MetroEnum.M3,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S2, "P.ta Venezia",MetroEnum.M1,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S2, "Dateo"));
		line_elements.add(new SElement(MetroEnum.S2, "P.ta Vittoria",MetroEnum.S5,MetroEnum.S6,MetroEnum.Lin,null,null,null));
		line_elements.add(new SElement(MetroEnum.S2, "Rogoredo",MetroEnum.M3,MetroEnum.S1,MetroEnum.S13,MetroEnum.R,null,null));
		return line_elements;
	}
	
	/**
	 * Returns line S3 elements
	 */
	public static ArrayList<SElement> getS3() {
		final ArrayList<SElement> line_elements = new ArrayList<SElement>();
		line_elements.add(new SElement(MetroEnum.S3, "Saronno",MetroEnum.S1,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S3, "Saronno Sud"));
		line_elements.add(new SElement(MetroEnum.S3, "Caronno P."));
		line_elements.add(new SElement(MetroEnum.S3, "Cesate"));
		line_elements.add(new SElement(MetroEnum.S3, "Garbagnate M."));
		line_elements.add(new SElement(MetroEnum.S3, "Garbagnate P.Groane"));
		line_elements.add(new SElement(MetroEnum.S3, "Bollate Nord"));
		line_elements.add(new SElement(MetroEnum.S3, "Bollate Centro"));
		line_elements.add(new SElement(MetroEnum.S3, "Novate M."));
		line_elements.add(new SElement(MetroEnum.S3, "Quarto Oggiaro"));
		line_elements.add(new SElement(MetroEnum.S3, "Bovisa",MetroEnum.S1,MetroEnum.S2,MetroEnum.S4,MetroEnum.S13,MetroEnum.R,null));
		line_elements.add(new SElement(MetroEnum.S3, "Domodossola"));
		line_elements.add(new SElement(MetroEnum.S3, "Mi Cadorna",MetroEnum.M1,MetroEnum.M2,MetroEnum.S4,MetroEnum.R,null,null));
		return line_elements;
	}
	
	/**
	 * Returns line S4 elements
	 */
	public static ArrayList<SElement> getS4() {
		final ArrayList<SElement> line_elements = new ArrayList<SElement>();
		line_elements.add(new SElement(MetroEnum.S4, "Camnago",MetroEnum.S11,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S4, "Seveso",MetroEnum.S2,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S4, "Cesano M."));
		line_elements.add(new SElement(MetroEnum.S4, "Bovisio M."));
		line_elements.add(new SElement(MetroEnum.S4, "Varedo"));
		line_elements.add(new SElement(MetroEnum.S4, "Palazzolo M.",MetroEnum.S2,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S4, "Paderno D."));
		line_elements.add(new SElement(MetroEnum.S4, "Cusano M."));
		line_elements.add(new SElement(MetroEnum.S4, "Cormano"));
		line_elements.add(new SElement(MetroEnum.S4, "Bruzzano"));
		line_elements.add(new SElement(MetroEnum.S4, "Affori"));
		line_elements.add(new SElement(MetroEnum.S4, "Bovisa",MetroEnum.S1,MetroEnum.S2,MetroEnum.S3,MetroEnum.S13,MetroEnum.R,MetroEnum.Mxp));
		line_elements.add(new SElement(MetroEnum.S4, "Domodossola"));
		line_elements.add(new SElement(MetroEnum.S4, "Cadorna",MetroEnum.M1,MetroEnum.M2,MetroEnum.S3,MetroEnum.R,MetroEnum.Mxp,null));
		return line_elements;
	}
	
	/**
	 * Returns line S5 elements
	 */
	public static ArrayList<SElement> getS5() {
		final ArrayList<SElement> line_elements = new ArrayList<SElement>();
		line_elements.add(new SElement(MetroEnum.S5, "Varese",MetroEnum.R,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S5, "Gazzada"));
		line_elements.add(new SElement(MetroEnum.S5, "Castronno"));
		line_elements.add(new SElement(MetroEnum.S5, "Albizzate"));
		line_elements.add(new SElement(MetroEnum.S5, "Cavaria"));
		line_elements.add(new SElement(MetroEnum.S5, "Gallarate",MetroEnum.R,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S5, "Busto Arsizio FS",MetroEnum.Mxp,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S5, "Legnano"));
		line_elements.add(new SElement(MetroEnum.S5, "Canegrate"));
		line_elements.add(new SElement(MetroEnum.S5, "Parabiago"));
		line_elements.add(new SElement(MetroEnum.S5, "Vanzago"));
		line_elements.add(new SElement(MetroEnum.S5, "Rho",MetroEnum.S6,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S5, "Rho-Fiera Milano",MetroEnum.M1,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S5, "Certosa"));
		line_elements.add(new SElement(MetroEnum.S5, "Villapizzone"));
		line_elements.add(new SElement(MetroEnum.S5, "Lancetti",MetroEnum.S1,MetroEnum.S2,MetroEnum.S13,null,null,null));
		line_elements.add(new SElement(MetroEnum.S5, "P.ta Garibaldi",MetroEnum.M2,MetroEnum.S8,MetroEnum.S11,MetroEnum.R,null,null));
		line_elements.add(new SElement(MetroEnum.S5, "Repubblica",MetroEnum.M3,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S5, "P.ta Venezia",MetroEnum.M1,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S5, "Dateo"));
		line_elements.add(new SElement(MetroEnum.S5, "P.ta Vittoria",MetroEnum.S1,MetroEnum.S2,MetroEnum.S13,MetroEnum.Lin,null,null));
		line_elements.add(new SElement(MetroEnum.S5, "Segrate"));
		line_elements.add(new SElement(MetroEnum.S5, "Pioltello",MetroEnum.S6,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S5, "Vignate"));
		line_elements.add(new SElement(MetroEnum.S5, "Melzo"));
		line_elements.add(new SElement(MetroEnum.S5, "Pozzuolo M."));
		line_elements.add(new SElement(MetroEnum.S5, "Trecella"));
		line_elements.add(new SElement(MetroEnum.S5, "Cassano d'Adda"));
		line_elements.add(new SElement(MetroEnum.S5, "Treviglio",MetroEnum.S6,MetroEnum.R,null,null,null,null));
		return line_elements;
	}
	
	/**
	 * Returns line S6 elements
	 */
	public static ArrayList<SElement> getS6() {
		final ArrayList<SElement> line_elements = new ArrayList<SElement>();
		line_elements.add(new SElement(MetroEnum.S6, "Novara",MetroEnum.R,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S6, "Trecate"));
		line_elements.add(new SElement(MetroEnum.S6, "Magenta"));
		line_elements.add(new SElement(MetroEnum.S6, "Corbetta"));
		line_elements.add(new SElement(MetroEnum.S6, "Vittuone"));
		line_elements.add(new SElement(MetroEnum.S6, "Pregnana M."));
		line_elements.add(new SElement(MetroEnum.S6, "Rho",MetroEnum.S5,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S6, "Rho-Fiera Milano",MetroEnum.M1,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S6, "Certosa"));
		line_elements.add(new SElement(MetroEnum.S6, "Villapizzone"));
		line_elements.add(new SElement(MetroEnum.S6, "Lancetti",MetroEnum.S1,MetroEnum.S2,MetroEnum.S13,null,null,null));
		line_elements.add(new SElement(MetroEnum.S6, "P.ta Garibaldi",MetroEnum.M2,MetroEnum.S8,MetroEnum.S11,MetroEnum.R,null,null));
		line_elements.add(new SElement(MetroEnum.S6, "Repubblica",MetroEnum.M3,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S6, "P.ta Venezia",MetroEnum.M1,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S6, "Dateo"));
		line_elements.add(new SElement(MetroEnum.S6, "P.ta Vittoria",MetroEnum.S1,MetroEnum.S2,MetroEnum.S13,MetroEnum.Lin,null,null));
		line_elements.add(new SElement(MetroEnum.S6, "Segrate"));
		line_elements.add(new SElement(MetroEnum.S6, "Pioltello",MetroEnum.S5,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S6, "Vignate"));
		line_elements.add(new SElement(MetroEnum.S6, "Melzo"));
		line_elements.add(new SElement(MetroEnum.S6, "Pozzuolo M."));
		line_elements.add(new SElement(MetroEnum.S6, "Trecella"));
		line_elements.add(new SElement(MetroEnum.S6, "Cassano d'Adda"));
		line_elements.add(new SElement(MetroEnum.S6, "Treviglio",MetroEnum.S5,MetroEnum.R,null,null,null,null));
		return line_elements;
	}
	
	/**
	 * Returns line S8 elements
	 */
	public static ArrayList<SElement> getS8() {
		final ArrayList<SElement> line_elements = new ArrayList<SElement>();
		line_elements.add(new SElement(MetroEnum.S8, "Lecco",MetroEnum.R,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S8, "Lecco Maggianico"));
		line_elements.add(new SElement(MetroEnum.S8, "Vercurago"));
		line_elements.add(new SElement(MetroEnum.S8, "Calolziocorte",MetroEnum.R,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S8, "Airuno"));
		line_elements.add(new SElement(MetroEnum.S8, "Olgiate M."));
		line_elements.add(new SElement(MetroEnum.S8, "Cernusco L."));
		line_elements.add(new SElement(MetroEnum.S8, "Osnago"));
		line_elements.add(new SElement(MetroEnum.S8, "Carnate",MetroEnum.R,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S8, "Arcore"));
		line_elements.add(new SElement(MetroEnum.S8, "Monza",MetroEnum.S9,MetroEnum.S11,MetroEnum.R,null,null,null));
		line_elements.add(new SElement(MetroEnum.S8, "Sesto (I Maggio)",MetroEnum.M1,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S8, "Greco",MetroEnum.S9,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S8, "P.Garibaldi",MetroEnum.M2,MetroEnum.S1,MetroEnum.S2,MetroEnum.S5,MetroEnum.S6,MetroEnum.S13, MetroEnum.S11, MetroEnum.R));
		return line_elements;
	}
	
	/**
	 * Returns line S9 elements
	 */
	public static ArrayList<SElement> getS9() {
		final ArrayList<SElement> line_elements = new ArrayList<SElement>();
		line_elements.add(new SElement(MetroEnum.S9, "Seregno",MetroEnum.S11,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S9, "Desio"));
		line_elements.add(new SElement(MetroEnum.S9, "Lissone"));
		line_elements.add(new SElement(MetroEnum.S9, "Monza",MetroEnum.S8,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S9, "Sesto (I Maggio)",MetroEnum.M1,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S9, "Greco",MetroEnum.S8,MetroEnum.S11,MetroEnum.R,null,null,null));
		line_elements.add(new SElement(MetroEnum.S9, "Lambrate",MetroEnum.M2,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S9, "P.ta Romana",MetroEnum.M3,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S9, "Romolo",MetroEnum.M2,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S9, "San Cristoforo",MetroEnum.R,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S9, "Corsico"));
		line_elements.add(new SElement(MetroEnum.S9, "Cesano Boscone"));
		line_elements.add(new SElement(MetroEnum.S9, "Trezzano sul Naviglio"));
		line_elements.add(new SElement(MetroEnum.S9, "Gaggiano"));
		line_elements.add(new SElement(MetroEnum.S9, "Albairate - Vermezzo",MetroEnum.R,null,null,null,null,null));
		return line_elements;
	}
	
	/**
	 * Returns line S10 elements
	 */
	public static ArrayList<SElement> getS13() {
		final ArrayList<SElement> line_elements = new ArrayList<SElement>();
		line_elements.add(new SElement(MetroEnum.S13, "Bovisa",MetroEnum.S1,MetroEnum.S2,MetroEnum.S3,MetroEnum.S4,MetroEnum.R,MetroEnum.Mxp));
		line_elements.add(new SElement(MetroEnum.S13, "Lancetti",MetroEnum.S5,MetroEnum.S6,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S13, "P.ta Garibaldi",MetroEnum.M2,MetroEnum.S8,MetroEnum.S11,MetroEnum.R,null,null));
		line_elements.add(new SElement(MetroEnum.S13, "Repubblica",MetroEnum.M3,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S13, "P.ta Venezia",MetroEnum.M1,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S13, "Dateo",MetroEnum.S1,MetroEnum.S2,MetroEnum.S5,MetroEnum.S6,null,null));
		line_elements.add(new SElement(MetroEnum.S13, "P.ta Vittoria",MetroEnum.S5,MetroEnum.S6,MetroEnum.Lin,null,null,null));
		line_elements.add(new SElement(MetroEnum.S13, "Rogoredo",MetroEnum.M3,MetroEnum.S1,MetroEnum.S2,MetroEnum.R,null,null));
		line_elements.add(new SElement(MetroEnum.S13, "Locate Triulzi"));
		line_elements.add(new SElement(MetroEnum.S13, "Villamaggiore"));
		line_elements.add(new SElement(MetroEnum.S13, "Certosa di Pavia"));
		line_elements.add(new SElement(MetroEnum.S13, "Pavia",MetroEnum.R,null,null,null,null,null));
		return line_elements;
	}
	
	/**
	 * Returns line S11 elements
	 */
	public static ArrayList<SElement> getS11() {
		final ArrayList<SElement> line_elements = new ArrayList<SElement>();
		line_elements.add(new SElement(MetroEnum.S11, "Chiasso",null,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S11, "Como S. Giovanni",MetroEnum.R,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S11, "Albate (Como Sud)",null,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S11, "Cucciago"));
		line_elements.add(new SElement(MetroEnum.S11, "Cant� Cermenate"));
		line_elements.add(new SElement(MetroEnum.S11, "Carimate"));
		line_elements.add(new SElement(MetroEnum.S11, "Camnago",MetroEnum.S4,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S11, "Seregno",MetroEnum.S9,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S11, "Desio"));
		line_elements.add(new SElement(MetroEnum.S11, "Lissone"));
		line_elements.add(new SElement(MetroEnum.S11, "Monza",MetroEnum.S7,MetroEnum.S8,MetroEnum.R,null,null,null));
		line_elements.add(new SElement(MetroEnum.S11, "Sesto (I Maggio)",MetroEnum.M1,null,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S11, "Greco",MetroEnum.S9,MetroEnum.R,null,null,null,null));
		line_elements.add(new SElement(MetroEnum.S11, "P.ta Garibaldi",MetroEnum.M2,MetroEnum.S1,MetroEnum.S2,MetroEnum.S5,MetroEnum.S6,MetroEnum.S8,MetroEnum.S13,MetroEnum.R));
		return line_elements;
	}
}
