/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm.data;


public class SElement extends ListElement{

	public SElement(MetroEnum prov, String name) {
		super();
		this.setName(name);
		this.setProv(prov);
	}

	public SElement(MetroEnum prov, String name, MetroEnum attr1, MetroEnum attr2, MetroEnum attr3,
			MetroEnum attr4, MetroEnum attr5, MetroEnum attr6) {
		super();
		this.setName(name);
		this.setAttr(attr1);
		this.setAttr(attr2);
		this.setAttr(attr3);
		this.setAttr(attr4);
		this.setAttr(attr5);
		this.setAttr(attr6);
		this.setProv(prov);
	}
	
	public SElement(MetroEnum prov, String name, MetroEnum attr1, MetroEnum attr2, MetroEnum attr3,
			MetroEnum attr4, MetroEnum attr5, MetroEnum attr6, MetroEnum attr7, MetroEnum attr8) {
		super();
		this.setName(name);
		this.setAttr(attr1);
		this.setAttr(attr2);
		this.setAttr(attr3);
		this.setAttr(attr4);
		this.setAttr(attr5);
		this.setAttr(attr6);
		this.setAttr(attr7);
		this.setAttr(attr8);
		this.setProv(prov);
	}


	
}