package com.sirowain.mm.data;

import java.util.ArrayList;

public class ListElement {

	private String name;
	private MetroEnum prov;
	private ArrayList<MetroEnum> attr = new ArrayList<MetroEnum>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<MetroEnum> getAttr() {
		return attr;
	}

	public void setAttr(MetroEnum attr) {
		this.attr.add(attr);
	}

	public MetroEnum getProv() {
		return prov;
	}

	public void setProv(MetroEnum prov) {
		this.prov = prov;
	}
}
