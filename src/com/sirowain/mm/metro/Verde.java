/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm.metro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.sirowain.mm.R;
import com.sirowain.mm.data.MetroColors;
import com.sirowain.mm.data.MetroData;
import com.sirowain.mm.data.MetroElement;
import com.sirowain.mm.data.MetroEnum;
import com.sirowain.mm.data.MetroUtils;
import com.sirowain.mm.data.MetroUtils.ViewHolder;

public class Verde extends Activity {

	Bitmap btm_first;
	Bitmap btm_last;
	Bitmap btm_norm;
	Bitmap btm_yellow;
	Bitmap btm_red;
	TextView text;
	String searchLine = "#";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setTitle(R.string.linea_verde);
		if (MetroData.ENABLE_ADS) {
			setContentView(R.layout.linea_metro);
			// Look up the AdView as a resource and load a request.
		    AdView adView = (AdView)this.findViewById(R.id.ad);
		    
		    Map<String, Object> extras = new HashMap<String, Object>();
		    extras.put("color_bg", "#8a8a8a");
		    extras.put("color_bg_top", "#CCCCCC");
		    extras.put("color_text", "#FFFFFF");

		    AdRequest adRequest = new AdRequest();
		    adRequest.setExtras(extras);
		    adView.loadAd(adRequest);
		} else {
			setContentView(R.layout.linea_metro_noads);
		}
		if (getIntent().getExtras()!=null) {
			searchLine = (String)(getIntent().getExtras().get(MetroData.APP_PACKAGE+".searchLine"));
		}
		
		 DisplayMetrics metrics = new DisplayMetrics();
		 getWindowManager().getDefaultDisplay().getMetrics(metrics);
		
		Resources res = getResources();
		btm_first = BitmapFactory.decodeResource(res, R.drawable.gen_start);
		btm_last = BitmapFactory.decodeResource(res, R.drawable.gen_end);
		btm_norm = BitmapFactory.decodeResource(res, R.drawable.gen_norm);
		btm_yellow = BitmapFactory.decodeResource(res, R.drawable.gen_giallo);
		btm_red = BitmapFactory.decodeResource(res, R.drawable.gen_rosso);

		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.mlogo);

		final ArrayList<MetroElement> listaMain = MetroData.getGreenLineMain();
		final ArrayList<MetroElement> listaBranch = MetroData.getGreenLineBranch();
		final ArrayList<MetroElement> lista2011 = MetroData.getGreenLineNew2011();

		ListView list = (ListView) findViewById(R.id.ListMetro);
		list.setItemsCanFocus(MetroData.ENABLE_LIST);

		ListAdapter listAdapter = new ListAdapter() {

			public View getView(int arg0, View layout_row, ViewGroup arg2) {
				ViewHolder holder;
				if (layout_row==null) {
					// CUSTOM VIEW
					layout_row = new LinearLayout(getApplicationContext());
					layout_row.setLayoutParams(new AbsListView.LayoutParams(
							LinearLayout.LayoutParams.FILL_PARENT,
							LinearLayout.LayoutParams.FILL_PARENT));
					layout_row.setHorizontalFadingEdgeEnabled(true);

					holder = new ViewHolder();
					holder.image1 = new ImageView(getApplicationContext());
					holder.image1.setBackgroundColor(Color.parseColor(MetroColors.verde));
					holder.text1 = new TextView(getApplicationContext());
					holder.text1.setLayoutParams(new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.FILL_PARENT));
					holder.text1.setTypeface(null, Typeface.BOLD);
					holder.text1.setTextColor(Color.BLACK);
					holder.text1.setGravity(Gravity.CENTER_VERTICAL);
					holder.text1.setPadding(MetroData.LIST_L_PAD,
							MetroData.LIST_T_PAD, MetroData.LIST_R_PAD,
							MetroData.LIST_B_PAD);
					((LinearLayout) layout_row).addView(holder.image1, 0);
					((LinearLayout) layout_row).addView(holder.text1, 1);
					
					// CUSTOM DOUBLE VIEW
					// LEFT
					holder.layoutl = new LinearLayout(getApplicationContext());
					holder.layoutl.setLayoutParams(new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.FILL_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT, 1));

					holder.imagel = new ImageView(getApplicationContext());
					holder.imagel.setBackgroundColor(Color.parseColor(MetroColors.verde));
					holder.textl = new TextView(getApplicationContext());
					holder.textl.setLayoutParams(new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.FILL_PARENT));
					holder.textl.setTypeface(null, Typeface.BOLD);
					holder.textl.setTextColor(Color.BLACK);
					holder.textl.setGravity(Gravity.CENTER_VERTICAL);
					holder.textl.setPadding(MetroData.LIST_L_PAD,
							MetroData.LIST_T_PAD, MetroData.LIST_R_PAD,
							MetroData.LIST_B_PAD);

					holder.layoutl.addView(holder.imagel, 0);
					holder.layoutl.addView(holder.textl, 1);

					// RIGHT
					holder.layoutr = new LinearLayout(getApplicationContext());
					holder.layoutr.setLayoutParams(new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.FILL_PARENT,
							LinearLayout.LayoutParams.WRAP_CONTENT, (float) 0.8));

					holder.imager = new ImageView(getApplicationContext());
					holder.imager.setBackgroundColor(Color.parseColor(MetroColors.verde));
					holder.textr = new TextView(getApplicationContext());
					holder.textr.setLayoutParams(new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.FILL_PARENT));
					holder.textr.setTypeface(null, Typeface.BOLD);
					holder.textr.setTextColor(Color.BLACK);
					holder.textr.setGravity(Gravity.CENTER_VERTICAL);
					holder.textr.setPadding(MetroData.LIST_L_PAD,
							MetroData.LIST_T_PAD, MetroData.LIST_R_PAD,
							MetroData.LIST_B_PAD);
					holder.layoutr.addView(holder.imager, 0);
					holder.layoutr.addView(holder.textr, 1);

					layout_row.setTag(holder);
				} else {
					holder = (ViewHolder)layout_row.getTag();
				}
				
				if (arg0 <= 6 || (arg0 >=11 && arg0 <=29)) {
					MetroElement lineElement = (MetroElement) listaMain.get(arg0);
					holder.image1.setImageBitmap(getImageForAttr(lineElement.getAttr().iterator().next()));
					holder.text1.setText(lineElement.getName());
					MetroUtils.selectSearchMetroD(holder, lineElement.getName(), null, searchLine);
					((LinearLayout)layout_row).removeAllViews();
					((LinearLayout)layout_row).addView(holder.image1);
					((LinearLayout)layout_row).addView(holder.text1);
					//layout_row.setClickable(true);
					//layout_row.setBackgroundResource(android.R.drawable.list_selector_background);
					return layout_row;
				}
				
				else if (arg0 >= 7 && arg0 <= 9) {
					MetroElement lineElement_l = (MetroElement) listaMain.get(arg0);
					MetroElement lineElement_r = (MetroElement) listaBranch.get(arg0 - 7);
					if (lineElement_l != null && lineElement_r != null) {
						Bitmap imageL = getImageForAttr(lineElement_l.getAttr().iterator().next());
						if (imageL != null) {
							holder.imagel.setImageBitmap(imageL);
						}
						if (!lineElement_l.getName().equals("")) {
							holder.textl.setText(lineElement_l.getName());
						}
						holder.imager.setImageBitmap(getImageForAttr(lineElement_r.getAttr().iterator().next()));
						holder.textr.setText(lineElement_r.getName());
						
						((LinearLayout)layout_row).removeAllViews();
						holder.layoutl.setClickable(MetroData.ENABLE_LIST);
						holder.layoutl.setBackgroundResource(android.R.drawable.list_selector_background);
						holder.layoutr.setClickable(MetroData.ENABLE_LIST);
						holder.layoutr.setBackgroundResource(android.R.drawable.list_selector_background);
						MetroUtils.selectSearchMetroD(holder, lineElement_l.getName(), lineElement_r.getName(), searchLine);
						layout_row.setBackgroundColor(Color.TRANSPARENT);
						((LinearLayout)layout_row).addView(holder.layoutl);
						((LinearLayout)layout_row).addView(holder.layoutr);
						//layout_row.setClickable(false);
						return layout_row;
					}
					return null;
				}
				
				else if (arg0 == 10) {
					int imgDim;
					if (getResources().getDisplayMetrics().densityDpi >= DisplayMetrics.DENSITY_HIGH){
						imgDim = 30;
					} else if (getResources().getDisplayMetrics().densityDpi >= DisplayMetrics.DENSITY_MEDIUM){
						imgDim = 20;
					} else {
						imgDim = 15;
					}
					((LinearLayout)layout_row).removeAllViews();
					LinearLayout lracc = new LinearLayout(getApplicationContext());
					holder.layoutl.setLayoutParams(new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.FILL_PARENT,
							LinearLayout.LayoutParams.FILL_PARENT, 1));
					holder.layoutl.setPadding(0, 0, 0, 0);
					ImageView raccImgSx = new ImageView(getApplicationContext());
					ImageView raccImgDx = new ImageView(getApplicationContext());
					raccImgSx.setImageResource(R.drawable.gen_noright);
					raccImgSx.setBackgroundColor(Color.parseColor(MetroColors.verde));
					raccImgSx.setPadding(0, 0, 0, 0);

					raccImgDx.setImageResource(R.drawable.gen_noleft_up);
					raccImgDx.setBackgroundColor(Color.parseColor(MetroColors.verde));
					holder.layoutl.removeAllViews();
					holder.layoutl.addView(raccImgSx);
					lracc.setBackgroundColor(Color.parseColor(MetroColors.verde));
					holder.layoutl.setGravity(Gravity.BOTTOM);
					holder.layoutl.setBaselineAligned(false);
					lracc.setBaselineAligned(false);
					holder.layoutl.addView(lracc, new AbsListView.LayoutParams(LayoutParams.FILL_PARENT,imgDim));
					holder.layoutl.setGravity(Gravity.BOTTOM);
					holder.layoutr.removeAllViews();
					holder.layoutr.addView(raccImgDx);
					((LinearLayout)layout_row).addView(holder.layoutl);
					((LinearLayout)layout_row).addView(holder.layoutr);
					((LinearLayout)layout_row).setLayoutParams(new AbsListView.LayoutParams(
							LayoutParams.FILL_PARENT,
							LayoutParams.FILL_PARENT));
					((LinearLayout)layout_row).setBaselineAligned(false);
					return layout_row;

				}
				else if (arg0 == 30) {
					int imgDim;
					if (getResources().getDisplayMetrics().densityDpi >= DisplayMetrics.DENSITY_HIGH){
						imgDim = 30;
					} else if (getResources().getDisplayMetrics().densityDpi >= DisplayMetrics.DENSITY_MEDIUM){
						imgDim = 20;
					} else {
						imgDim = 15;
					}
					((LinearLayout)layout_row).removeAllViews();
					LinearLayout lracc = new LinearLayout(getApplicationContext());
					ImageView raccImgSx = new ImageView(getApplicationContext());
					ImageView raccImgDx = new ImageView(getApplicationContext());
					raccImgSx.setImageResource(R.drawable.gen_noright);
					raccImgSx.setBackgroundColor(Color.parseColor(MetroColors.verde));
					raccImgSx.setPadding(0, 0, 0, 0);
					raccImgDx.setImageResource(R.drawable.gen_noleft);
					raccImgDx.setBackgroundColor(Color.parseColor(MetroColors.verde));
					holder.layoutl.removeAllViews();
					holder.layoutl.addView(raccImgSx);
					lracc.setBackgroundColor(Color.parseColor(MetroColors.verde));
					lracc.setGravity(Gravity.CENTER_VERTICAL);
					holder.layoutl.setBaselineAligned(false);
					lracc.setBaselineAligned(false);
					holder.layoutl.addView(lracc, new AbsListView.LayoutParams(LayoutParams.FILL_PARENT,imgDim));
					lracc.setGravity(Gravity.CENTER_VERTICAL);
					holder.layoutr.removeAllViews();
					holder.layoutr.addView(raccImgDx);
					((LinearLayout)layout_row).addView(holder.layoutl);
					((LinearLayout)layout_row).addView(holder.layoutr);
					((LinearLayout)layout_row).setLayoutParams(new AbsListView.LayoutParams(
							LayoutParams.FILL_PARENT,
							LayoutParams.FILL_PARENT));
					((LinearLayout)layout_row).setBaselineAligned(false);
					return layout_row;

				}
				
				else if (arg0 == 31) {
					MetroElement lineElement_r = (MetroElement) lista2011.get(arg0 - 31);
					MetroElement lineElement_l = (MetroElement) listaMain.get(arg0);
					if (lineElement_l != null && lineElement_r != null) {
						Bitmap imageL = getImageForAttr(lineElement_l.getAttr().iterator().next());
						if (imageL != null) {
							holder.imagel.setImageBitmap(imageL);
						}
						if (!lineElement_l.getName().equals("")) {
							holder.textl.setText(lineElement_l.getName());
						}
						holder.imager.setImageBitmap(getImageForAttr(lineElement_r.getAttr().iterator().next()));
						holder.textr.setText(lineElement_r.getName());
						MetroUtils.selectSearchMetroD(holder, lineElement_l.getName(), lineElement_r.getName(), searchLine);
						((LinearLayout)layout_row).removeAllViews();
						holder.layoutl.setClickable(MetroData.ENABLE_LIST);
						holder.layoutl.setBackgroundResource(android.R.drawable.list_selector_background);
						holder.layoutr.setClickable(MetroData.ENABLE_LIST);
						holder.layoutr.setBackgroundResource(android.R.drawable.list_selector_background);
						layout_row.setBackgroundColor(Color.TRANSPARENT);
						((LinearLayout)layout_row).addView(holder.layoutl);
						((LinearLayout)layout_row).addView(holder.layoutr);
						return layout_row;
					}
					return null;
				}
				
				else {
					MetroElement lineElement_r = (MetroElement) lista2011.get(arg0-31);
					if (lineElement_r != null) {
						holder.imager.setImageBitmap(getImageForAttr(lineElement_r.getAttr().iterator().next()));
						holder.textr.setText(lineElement_r.getName());
						MetroUtils.selectSearchMetroD(holder, null, lineElement_r.getName(), searchLine);
						holder.imagel.setImageBitmap(null);
						holder.textl.setText("");
						((LinearLayout)layout_row).removeAllViews();
						((LinearLayout)layout_row).addView(holder.layoutl);
						((LinearLayout)layout_row).addView(holder.layoutr);
						holder.layoutr.setClickable(MetroData.ENABLE_LIST);
						return layout_row;
					}
					return null;
				}

			}

			public long getItemId(int arg0) {
				return arg0;
			}

			public Object getItem(int arg0) {
				return listaMain.get(arg0);
			}

			public int getCount() {
				return listaMain.size()+1;
			}

			private Bitmap getImageForAttr(MetroEnum attr) {
				if (attr==MetroEnum.ELEMENT_FIRST) {
					return btm_first;
				} else if (attr==MetroEnum.ELEMENT_LAST) {
					return btm_last;
				} else if (attr==MetroEnum.ELEMENT_NORM) {
					return btm_norm;
				} else if (attr==MetroEnum.ELEMENT_RED) {
					return btm_red;
				} else if (attr==MetroEnum.ELEMENT_YELLOW) {
					return btm_yellow;
				}
				return null;
			}

			public int getItemViewType(int arg0) {
				if (arg0 >= 7 && arg0 <= 9){ return 1; }
				else if (arg0 == 10) { return 2; }
				else if (arg0 == 30) { return 4; }
				else if (arg0 == 31){ return 1; }
				else if (arg0 == 32) { return 3; }
				return 0;
			}

			public int getViewTypeCount() {
				return 5;
			}

			public boolean hasStableIds() {
				return true;
			}

			public boolean isEmpty() {
				return false;
			}

			public void registerDataSetObserver(DataSetObserver arg0) {
				
			}

			public void unregisterDataSetObserver(DataSetObserver arg0) {
				
			}

			public boolean areAllItemsEnabled() {
				return MetroData.ENABLE_LIST;
			}

			public boolean isEnabled(int arg0) {
				if (arg0 >= 7 && arg0 <= 9) {
					return false;
				}
				return MetroData.ENABLE_LIST;
			}
			
		};
		
//		if (TabMetro.orig!=null && TabMetro.dest!=null) {
//			text = new TextView(getApplicationContext());
//			text.setText("From " + TabMetro.orig + " to " + TabMetro.dest);
//			list.addHeaderView(text);
//		}
//		
//		list.setOnItemLongClickListener(new ListView.OnItemLongClickListener() {
//			
//			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
//					int arg2, long arg3) {
//				if(TabMetro.orig.equals("default")) {
//					TabMetro.orig = ((MetroElement)listaMain.get(arg2)).getName();
//				} else if (TabMetro.dest.equals("default")) {
//					TabMetro.dest = ((MetroElement)listaMain.get(arg2)).getName();
//				} else if (!TabMetro.orig.equals("default") && !TabMetro.dest.equals("default")) {
//					TabMetro.orig = ((MetroElement)listaMain.get(arg2)).getName();
//					TabMetro.dest = "default";
//				}
//				refreshDest();
//				return true;
//			}
//		});

		list.setAdapter(listAdapter);
	}
	
//	private void refreshDest() {
//		text.setText("From " + TabMetro.orig + " to " + TabMetro.dest);
//	}
//
//	@Override
//	protected void onResume() {
//		super.onResume();
//		refreshDest();
//	}
//	
	
}
