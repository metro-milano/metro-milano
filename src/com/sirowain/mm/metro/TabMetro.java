/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm.metro;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.sirowain.mm.R;
import com.sirowain.mm.data.MetroData;

public class TabMetro extends TabActivity {

	static String dest = "default";
	static String orig = "default";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.metro_tab);

		Integer defaultTab = 0;
		if (getIntent().getExtras()!=null) {
			defaultTab = (Integer)(getIntent().getExtras().get(MetroData.APP_PACKAGE+".defaultTab"));
		}
		
		TabHost tabHost = getTabHost();
		TabHost.TabSpec spec;
		Intent intent;

		// LINEA ROSSA
		intent = new Intent().setClass(this, Rossa.class);
		intent.putExtras(getIntent());
		spec = tabHost
				.newTabSpec("Rossa")
				.setIndicator(
						new MyView(getApplicationContext(),
								R.drawable.tab_rossa_res_2, R.string.rossa,
								R.drawable.btn_tab_metro)).setContent(intent);
		tabHost.addTab(spec);

		// LINEA VERDE
		intent = new Intent().setClass(this, Verde.class);
		intent.putExtras(getIntent());
		spec = tabHost
				.newTabSpec("Verde")
				.setIndicator(
						new MyView(getApplicationContext(),
								R.drawable.tab_verde_res_2, R.string.verde,
								R.drawable.btn_tab_metro)).setContent(intent);
		tabHost.addTab(spec);
		
		// LINEA GIALLA
		intent = new Intent().setClass(this, Gialla.class);
		intent.putExtras(getIntent());
		spec = tabHost
				.newTabSpec("Gialla")
				.setIndicator(
						new MyView(getApplicationContext(),
								R.drawable.tab_gialla_res_2, R.string.gialla,
								R.drawable.btn_tab_metro)).setContent(intent);
		tabHost.addTab(spec);
		
		tabHost.setCurrentTab(defaultTab.intValue());

	}

	private class MyView extends LinearLayout {
		public MyView(Context c, int drawable, int label, int background) {
			super(c);
			setBackgroundResource(background);
			ImageView iv = new ImageView(c);
			TextView tv = new TextView(c);
//			setLayoutParams(new LinearLayout.LayoutParams(
//					LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			tv.setTextColor(Color.BLACK);
			iv.setImageResource(drawable);
			tv.setText(label);
			tv.setGravity(Gravity.CENTER_HORIZONTAL);

			setOrientation(LinearLayout.HORIZONTAL);
			iv.setPadding(2, 2, 2, 2);
			addView(iv);
			addView(tv);
		}
	}
}
