/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm.metro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.sirowain.mm.R;
import com.sirowain.mm.data.MetroColors;
import com.sirowain.mm.data.MetroData;
import com.sirowain.mm.data.MetroElement;
import com.sirowain.mm.data.MetroEnum;
import com.sirowain.mm.data.MetroUtils;
import com.sirowain.mm.data.MetroUtils.ViewHolder;

public class Gialla extends Activity {

	Bitmap btm_first;
	Bitmap btm_last;
	Bitmap btm_norm;
	Bitmap btm_red;
	Bitmap btm_green;
	TextView text;
	String searchLine = "#";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setTitle(R.string.linea_gialla);
		if (MetroData.ENABLE_ADS) {
			setContentView(R.layout.linea_metro);
			// Look up the AdView as a resource and load a request.
		    AdView adView = (AdView)this.findViewById(R.id.ad);
		    
		    Map<String, Object> extras = new HashMap<String, Object>();
		    extras.put("color_bg", "#8a8a8a");
		    extras.put("color_bg_top", "#CCCCCC");
		    extras.put("color_text", "#FFFFFF");

		    AdRequest adRequest = new AdRequest();
		    adRequest.setExtras(extras);
		    adView.loadAd(adRequest);
		} else {
			setContentView(R.layout.linea_metro_noads);
		}
		if (getIntent().getExtras()!=null) {
			searchLine = (String)(getIntent().getExtras().get(MetroData.APP_PACKAGE+".searchLine"));
		}
		
		Resources res = getResources();
		btm_first = BitmapFactory.decodeResource(res, R.drawable.gen_start);
		btm_last = BitmapFactory.decodeResource(res, R.drawable.gen_end);
		btm_norm = BitmapFactory.decodeResource(res, R.drawable.gen_norm);
		btm_red = BitmapFactory.decodeResource(res, R.drawable.gen_rosso);
		btm_green = BitmapFactory.decodeResource(res, R.drawable.gen_verde);

		setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.mlogo);

		final ArrayList<MetroElement> listaMain = MetroData.getYellowLineMain();

		ListView list = (ListView) findViewById(R.id.ListMetro);
		list.setItemsCanFocus(MetroData.ENABLE_LIST);

		ListAdapter listAdapter = new ListAdapter() {

			public View getView(int arg0, View layout_row, ViewGroup arg2) {
				ViewHolder holder;
				if (layout_row == null) {
					// CUSTOM VIEW
					layout_row = new LinearLayout(getApplicationContext());
					layout_row.setLayoutParams(new AbsListView.LayoutParams(
							LinearLayout.LayoutParams.FILL_PARENT,
							LinearLayout.LayoutParams.FILL_PARENT));
					layout_row.setHorizontalFadingEdgeEnabled(false);

					holder = new ViewHolder();
					holder.image1 = new ImageView(getApplicationContext());
					holder.image1.setBackgroundColor(Color
							.parseColor(MetroColors.giallo));
					holder.text1 = new TextView(getApplicationContext());
					holder.text1.setLayoutParams(new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.FILL_PARENT));
					holder.text1.setPadding(MetroData.LIST_L_PAD,
							MetroData.LIST_T_PAD, MetroData.LIST_R_PAD,
							MetroData.LIST_B_PAD);
					holder.text1.setTypeface(null, Typeface.BOLD);
					holder.text1.setTextColor(Color.BLACK);
					holder.text1.setGravity(Gravity.CENTER_VERTICAL);
					((LinearLayout) layout_row).addView(holder.image1, 0);
					((LinearLayout) layout_row).addView(holder.text1, 1);

					layout_row.setTag(holder);
				} else {
					holder = (ViewHolder) layout_row.getTag();
				}

				MetroElement lineElement = (MetroElement) listaMain.get(arg0);
				holder.image1.setImageBitmap(getImageForAttr(lineElement
						.getAttr().iterator().next()));
				holder.text1.setText(lineElement.getName());
				if (lineElement.getName().equals(searchLine)) {
					SpannableString underlineString = new SpannableString(holder.text1.getText());
					MetroUtils.setSearchStyle(underlineString);
					holder.text1.setText(underlineString);
				}
				((LinearLayout) layout_row).removeAllViews();
				((LinearLayout) layout_row).addView(holder.image1);
				((LinearLayout) layout_row).addView(holder.text1);
				return layout_row;

			}

			public long getItemId(int arg0) {
				return arg0;
			}

			public Object getItem(int arg0) {
				return listaMain.get(arg0);
			}

			public int getCount() {
				return listaMain.size();
			}

			private Bitmap getImageForAttr(MetroEnum attr) {
				if (attr == MetroEnum.ELEMENT_FIRST) {
					return btm_first;
				} else if (attr == MetroEnum.ELEMENT_LAST) {
					return btm_last;
				} else if (attr == MetroEnum.ELEMENT_NORM) {
					return btm_norm;
				} else if (attr == MetroEnum.ELEMENT_GREEN) {
					return btm_green;
				} else if (attr == MetroEnum.ELEMENT_RED) {
					return btm_red;
				}
				return null;
			}

			public int getItemViewType(int arg0) {
				return 0;
			}

			public int getViewTypeCount() {
				return 1;
			}

			public boolean hasStableIds() {
				return true;
			}

			public boolean isEmpty() {
				return false;
			}

			public void registerDataSetObserver(DataSetObserver arg0) {

			}

			public void unregisterDataSetObserver(DataSetObserver arg0) {

			}

			public boolean areAllItemsEnabled() {
				return MetroData.ENABLE_LIST;
			}

			public boolean isEnabled(int arg0) {
				return MetroData.ENABLE_LIST;
			}
		};

		// if (TabMetro.orig!=null && TabMetro.dest!=null) {
		// text = new TextView(getApplicationContext());
		// text.setText("From " + TabMetro.orig + " to " + TabMetro.dest);
		// list.addHeaderView(text);
		// }
		//		
		// list.setOnItemLongClickListener(new
		// ListView.OnItemLongClickListener() {
		//			
		// public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
		// int arg2, long arg3) {
		// if(TabMetro.orig.equals("default")) {
		// TabMetro.orig = ((MetroElement)listaMain.get(arg2)).getName();
		// } else if (TabMetro.dest.equals("default")) {
		// TabMetro.dest = ((MetroElement)listaMain.get(arg2)).getName();
		// } else if (!TabMetro.orig.equals("default") &&
		// !TabMetro.dest.equals("default")) {
		// TabMetro.orig = ((MetroElement)listaMain.get(arg2)).getName();
		// TabMetro.dest = "default";
		// }
		// refreshDest();
		// return true;
		// }
		// });

		list.setAdapter(listAdapter);
	}

	// private void refreshDest() {
	// text.setText("From " + TabMetro.orig + " to " + TabMetro.dest);
	// }
	//
	// @Override
	// protected void onResume() {
	// super.onResume();
	// refreshDest();
	// }

}
