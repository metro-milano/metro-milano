/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm.passante;

import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sirowain.mm.R;
import com.sirowain.mm.data.SData;

public class PassanteMain extends Activity {

	Intent linea;
	Bitmap s1;
	Bitmap s2;
	Bitmap s3;
	Bitmap s4;
	Bitmap s5;
	Bitmap s6;
	Bitmap s8;
	Bitmap s9;
	Bitmap s10;
	Bitmap s11;
	Bitmap s13;
	
	static class ViewHolder {
		ImageView image1;
		TextView text;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		linea = new Intent(this, LineaSActivity.class);
		
		RelativeLayout layout = new RelativeLayout(getApplicationContext());
		layout.setLayoutParams(new AbsListView.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT));
		layout.setHorizontalFadingEdgeEnabled(false);
		layout.setBackgroundColor(Color.WHITE);
		setContentView(layout);
		
		Resources res = getResources();
		s1 = BitmapFactory.decodeResource(res, R.drawable.s1);
		s2 = BitmapFactory.decodeResource(res, R.drawable.s2);
		s3 = BitmapFactory.decodeResource(res, R.drawable.s3);
		s4 = BitmapFactory.decodeResource(res, R.drawable.s4);
		s5 = BitmapFactory.decodeResource(res, R.drawable.s5);
		s6 = BitmapFactory.decodeResource(res, R.drawable.s6);
		s8 = BitmapFactory.decodeResource(res, R.drawable.s8);
		s9 = BitmapFactory.decodeResource(res, R.drawable.s9);
		s10 = BitmapFactory.decodeResource(res, R.drawable.s10);
		s11 = BitmapFactory.decodeResource(res, R.drawable.s11);
		s13 = BitmapFactory.decodeResource(res, R.drawable.s13);
		
		final Map<String,String> listaMain = SData.getNomi();

		ListView list = new ListView(getApplicationContext());
		list.setLayoutParams(new AbsListView.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT));
		list.setItemsCanFocus(true);
		LayoutInflater infl = getLayoutInflater();
		LinearLayout header = (LinearLayout) infl.inflate(R.layout.passante_header, null);
		TextView text = (TextView)header.findViewById(R.id.headerText);
		text.setTextScaleX((float)1.5);
		text.setTextColor(Color.WHITE);
		RelativeLayout.LayoutParams ps = new RelativeLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT);
		ps.addRule(RelativeLayout.BELOW, R.id.headerLayout);
		layout.addView(header,
				RelativeLayout.LayoutParams.FILL_PARENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
//		BitmapDrawable s = new BitmapDrawable(BitmapFactory.decodeResource(res, R.drawable.divider));
//		list.setDivider(s);
		list.setDividerHeight(0);
		list.setCacheColorHint(Color.WHITE);

		ListAdapter listAdapter = new ListAdapter() {

			public boolean areAllItemsEnabled() {
				return true;
			}

			public boolean isEnabled(int position) {
				return true;
			}

			public int getCount() {
				return listaMain.size();
			}

			public Object getItem(int arg0) {
				return null;
			}

			public long getItemId(int arg0) {
				return arg0;
			}

			public int getItemViewType(int arg0) {
				return 0;
			}

			public View getView(int index, View layout, ViewGroup viewGroup) {
				ViewHolder holder;
				if (layout==null) {
					// CUSTOM VIEW
					layout = new LinearLayout(getApplicationContext());
					layout.setLayoutParams(new AbsListView.LayoutParams(
							LinearLayout.LayoutParams.FILL_PARENT,
							LinearLayout.LayoutParams.FILL_PARENT));
					layout.setHorizontalFadingEdgeEnabled(false);

					holder = new ViewHolder();
					holder.image1 = new ImageView(getApplicationContext());
					holder.text = new TextView(getApplicationContext());
					holder.text.setPadding(2, 0, 0, 0);
					holder.text.setTextColor(Color.BLACK);
					holder.text.setLayoutParams(new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.FILL_PARENT));
					holder.text.setTypeface(null, Typeface.BOLD);
					holder.text.setGravity(Gravity.CENTER_VERTICAL);
					((LinearLayout) layout).addView(holder.image1, 0);
					((LinearLayout) layout).addView(holder.text, 1);
					layout.setTag(holder);
				} else {
					holder = (ViewHolder)layout.getTag();
				}
				
				switch (index) {
				case 0:
					holder.image1.setImageBitmap(s1);
					holder.text.setText(listaMain.get("S1"));
					layout.setId(1);
					break;
				case 1:
					holder.image1.setImageBitmap(s2);
					holder.text.setText(listaMain.get("S2"));
					layout.setId(2);
					break;
				case 2:
					holder.image1.setImageBitmap(s3);
					holder.text.setText(listaMain.get("S3"));
					layout.setId(3);
					break;
				case 3:
					holder.image1.setImageBitmap(s4);
					holder.text.setText(listaMain.get("S4"));
					layout.setId(4);
					break;
				case 4:
					holder.image1.setImageBitmap(s5);
					holder.text.setText(listaMain.get("S5"));
					layout.setId(5);
					break;
				case 5:
					holder.image1.setImageBitmap(s6);
					holder.text.setText(listaMain.get("S6"));
					layout.setId(6);
					break;
				case 6:
					holder.image1.setImageBitmap(s8);
					holder.text.setText(listaMain.get("S8"));
					layout.setId(8);
					break;
				case 7:
					holder.image1.setImageBitmap(s9);
					holder.text.setText(listaMain.get("S9"));
					layout.setId(9);
					break;
				case 8:
					holder.image1.setImageBitmap(s10);
					holder.text.setText(getString(R.string.warning));
					layout.setId(10);
					break;
				case 9:
					holder.image1.setImageBitmap(s11);
					holder.text.setText(listaMain.get("S11"));
					layout.setId(11);
					break;
				case 10:
					holder.image1.setImageBitmap(s13);
					holder.text.setText(listaMain.get("S13"));
					layout.setId(13);
					break;
				default:
					break;
				}
				return layout;
			}

			public int getViewTypeCount() {
				return 1;
			}

			public boolean hasStableIds() {
				return false;
			}

			public boolean isEmpty() {
				return false;
			}

			public void registerDataSetObserver(DataSetObserver arg0) {
			}

			public void unregisterDataSetObserver(DataSetObserver arg0) {
			}
		};
		list.setAdapter(listAdapter);
		list.setOnItemClickListener(new ListView.OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View vista, int arg2,
					long arg3) {
				if (vista.getId()>0 && vista.getId() != 10) {
					linea.putExtra("linea", vista.getId());
					startActivity(linea);
				}
			}
		});
		
		layout.addView(list, ps);
	}
}
