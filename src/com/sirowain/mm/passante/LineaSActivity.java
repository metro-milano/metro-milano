/* This file is part of MilanMetro.
 * 
 * MilanMetro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MilanMetro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MilanMetro.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.mm.passante;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.sirowain.mm.R;
import com.sirowain.mm.data.MetroColors;
import com.sirowain.mm.data.MetroData;
import com.sirowain.mm.data.MetroEnum;
import com.sirowain.mm.data.MetroUtils;
import com.sirowain.mm.data.SData;
import com.sirowain.mm.data.SElement;

public class LineaSActivity extends Activity {

	int linea;
	Bitmap btm_first;
	Bitmap btm_last;
	Bitmap btm_norm;
	Bitmap btm_imp;
	Bitmap s1;
	Bitmap s2;
	Bitmap s3;
	Bitmap s4;
	Bitmap s5;
	Bitmap s6;
	Bitmap s8;
	Bitmap s9;
	Bitmap s11;
	Bitmap s13;
	Bitmap m1;
	Bitmap m2;
	Bitmap m3;
	Bitmap r;
	Bitmap lin;
	Bitmap mxp;
	TextView text;
	String colore_linea;
	ArrayList<SElement> listaMain;
	String searchLine = "#";
	
	static class ViewHolder {
		LinearLayout layout_attr;
		ImageView image1;
		TextView text2;
		TextView text1;
		ImageView at1;
		ImageView at2;
		ImageView at3;
		ImageView at4;
		ImageView at5;
		ImageView at6;
		ImageView at8;
		ImageView at9;
		ImageView at11;
		ImageView at13;
		ImageView atM1;
		ImageView atM2;
		ImageView atM3;
		ImageView atR;
		ImageView atLin;
		ImageView atMxp;
		Map<MetroEnum,ImageView> attrs = new HashMap<MetroEnum,ImageView>();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (MetroData.ENABLE_ADS) {
			setContentView(R.layout.linea_s);
			// Look up the AdView as a resource and load a request.
		    AdView adView = (AdView)this.findViewById(R.id.ad);
		    
		    Map<String, Object> extras = new HashMap<String, Object>();
		    extras.put("color_bg", "#205fa4");
		    extras.put("color_bg_top", "#CCCCCC");
		    extras.put("color_text", "#FFFFFF");

		    AdRequest adRequest = new AdRequest();
		    adRequest.setExtras(extras);
		    adView.loadAd(adRequest);
		} else {
			setContentView(R.layout.linea_s_noads);
		}
		if (getIntent().getExtras()!=null) {
			searchLine = (String)(getIntent().getExtras().get(MetroData.APP_PACKAGE+".searchLine"));
		}
		Resources res = getResources();
		btm_first = BitmapFactory.decodeResource(res, R.drawable.gen_start_s);
		btm_last = BitmapFactory.decodeResource(res, R.drawable.gen_end_s);
		btm_norm = BitmapFactory.decodeResource(res, R.drawable.gen_norm_s);
		btm_imp = BitmapFactory.decodeResource(res, R.drawable.gen_imp_s);
		s1 = BitmapFactory.decodeResource(res, R.drawable.s1);
		s2 = BitmapFactory.decodeResource(res, R.drawable.s2);
		s3 = BitmapFactory.decodeResource(res, R.drawable.s3);
		s4 = BitmapFactory.decodeResource(res, R.drawable.s4);
		s5 = BitmapFactory.decodeResource(res, R.drawable.s5);
		s6 = BitmapFactory.decodeResource(res, R.drawable.s6);
		s8 = BitmapFactory.decodeResource(res, R.drawable.s8);
		s9 = BitmapFactory.decodeResource(res, R.drawable.s9);
		s11 = BitmapFactory.decodeResource(res, R.drawable.s11);
		s13 = BitmapFactory.decodeResource(res, R.drawable.s13);
		m1 = BitmapFactory.decodeResource(res, R.drawable.m1);
		m2 = BitmapFactory.decodeResource(res, R.drawable.m2);
		m3 = BitmapFactory.decodeResource(res, R.drawable.m3);
		r = BitmapFactory.decodeResource(res, R.drawable.line_r);
		lin = BitmapFactory.decodeResource(res, R.drawable.linate);
		mxp = BitmapFactory.decodeResource(res, R.drawable.malpensa);

		//HEADER
		LinearLayout header = (LinearLayout)findViewById(R.id.header);
		ImageView img2 = (ImageView)header.findViewById(R.id.headerSimg);
		TextView text = (TextView)header.findViewById(R.id.headerText);
		text.setTextColor(Color.WHITE);
		
		linea = getIntent().getIntExtra("linea",0);
		if (linea==0) {
			linea = convertNameToIndex();
		}
		switch (linea) {
		case 1:
			text.setText(SData.getName("S1"));
			img2.setImageBitmap(s1);
			listaMain = SData.getS1();
			colore_linea = MetroColors.s1;
			break;
		case 2:
			text.setText(SData.getName("S2"));
			img2.setImageBitmap(s2);
			listaMain = SData.getS2();
			colore_linea = MetroColors.s2;
			break;
		case 3:
			text.setText(SData.getName("S3"));
			img2.setImageBitmap(s3);
			listaMain = SData.getS3();
			colore_linea = MetroColors.s3;
			break;
		case 4:
			text.setText(SData.getName("S4"));
			img2.setImageBitmap(s4);
			listaMain = SData.getS4();
			colore_linea = MetroColors.s4;
			break;
		case 5:
			text.setText(SData.getName("S5"));
			img2.setImageBitmap(s5);
			listaMain = SData.getS5();
			colore_linea = MetroColors.s5;
			break;
		case 6:
			text.setText(SData.getName("S6"));
			img2.setImageBitmap(s6);
			listaMain = SData.getS6();
			colore_linea = MetroColors.s6;
			break;
		case 8:
			text.setText(SData.getName("S8"));
			img2.setImageBitmap(s8);
			listaMain = SData.getS8();
			colore_linea = MetroColors.s8;
			break;
		case 9:
			text.setText(SData.getName("S9"));
			img2.setImageBitmap(s9);
			listaMain = SData.getS9();
			colore_linea = MetroColors.s9;
			break;
		case 11:
			text.setText(SData.getName("S11"));
			img2.setImageBitmap(s11);
			listaMain = SData.getS11();
			colore_linea = MetroColors.s11;
			break;
		case 13:
			text.setText(SData.getName("S13"));
			img2.setImageBitmap(s13);
			listaMain = SData.getS13();
			colore_linea = MetroColors.s13;
			break;
		default:
			break;
		}
		
		ListView list = (ListView) findViewById(R.id.ListLineaS);
		list.setItemsCanFocus(SData.ENABLE_LIST);

		ListAdapter listAdapter = new ListAdapter() {

			public View getView(int index, View layout, ViewGroup arg2) {
				ViewHolder holder;
				if (layout==null) {
					// CUSTOM VIEW
					layout = new LinearLayout(getApplicationContext());
					layout.setLayoutParams(new AbsListView.LayoutParams(
							LinearLayout.LayoutParams.FILL_PARENT,
							LinearLayout.LayoutParams.FILL_PARENT));
					layout.setHorizontalFadingEdgeEnabled(false);
					((LinearLayout)layout).setBaselineAligned(false);
					holder = new ViewHolder();
					holder.layout_attr = new LinearLayout(getApplicationContext());
					holder.layout_attr.setLayoutParams(new AbsListView.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.FILL_PARENT));
					holder.layout_attr.setBaselineAligned(false);
					int imgDim;
					if (getResources().getDisplayMetrics().densityDpi >= DisplayMetrics.DENSITY_HIGH){
						imgDim = 32;
					} else if (getResources().getDisplayMetrics().densityDpi >= DisplayMetrics.DENSITY_MEDIUM){
						imgDim = 24;
					} else {
						imgDim = 18;
					}
					System.out.println(layout.getHeight());	
					
					holder.at1 = new ImageView(getApplicationContext());
					holder.at1.setImageBitmap(Bitmap.createScaledBitmap(s1, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.S1, holder.at1);
					holder.at2 = new ImageView(getApplicationContext());
					holder.at2.setImageBitmap(Bitmap.createScaledBitmap(s2, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.S2, holder.at2);
					holder.at3 = new ImageView(getApplicationContext());
					holder.at3.setImageBitmap(Bitmap.createScaledBitmap(s3, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.S3, holder.at3);
					holder.at4 = new ImageView(getApplicationContext());
					holder.at4.setImageBitmap(Bitmap.createScaledBitmap(s4, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.S4, holder.at4);
					holder.at5 = new ImageView(getApplicationContext());
					holder.at5.setImageBitmap(Bitmap.createScaledBitmap(s5, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.S5, holder.at5);
					holder.at6 = new ImageView(getApplicationContext());
					holder.at6.setImageBitmap(Bitmap.createScaledBitmap(s6, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.S6, holder.at6);
					holder.at8 = new ImageView(getApplicationContext());
					holder.at8.setImageBitmap(Bitmap.createScaledBitmap(s8, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.S8, holder.at8);
					holder.at9 = new ImageView(getApplicationContext());
					holder.at9.setImageBitmap(Bitmap.createScaledBitmap(s9, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.S9, holder.at9);
					holder.at11 = new ImageView(getApplicationContext());
					holder.at11.setImageBitmap(Bitmap.createScaledBitmap(s11, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.S11, holder.at11);
					holder.atM1 = new ImageView(getApplicationContext());
					holder.atM1.setImageBitmap(Bitmap.createScaledBitmap(m1, imgDim, imgDim, true));
					holder.at13 = new ImageView(getApplicationContext());
					holder.at13.setImageBitmap(Bitmap.createScaledBitmap(s13, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.S13, holder.at13);
					holder.attrs.put(MetroEnum.M1, holder.atM1);
					holder.atM2 = new ImageView(getApplicationContext());
					holder.atM2.setImageBitmap(Bitmap.createScaledBitmap(m2, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.M2, holder.atM2);
					holder.atM3 = new ImageView(getApplicationContext());
					holder.atM3.setImageBitmap(Bitmap.createScaledBitmap(m3, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.M3, holder.atM3);
					holder.atR = new ImageView(getApplicationContext());
					holder.atR.setImageBitmap(Bitmap.createScaledBitmap(r, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.R, holder.atR);
					holder.atLin = new ImageView(getApplicationContext());
					holder.atLin.setImageBitmap(Bitmap.createScaledBitmap(lin, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.Lin, holder.atLin);
					holder.atMxp = new ImageView(getApplicationContext());
					holder.atMxp.setImageBitmap(Bitmap.createScaledBitmap(mxp, imgDim, imgDim, true));
					holder.attrs.put(MetroEnum.Mxp, holder.atMxp);
					holder.image1 = new ImageView(getApplicationContext());
					holder.image1.setBackgroundColor(Color.parseColor(colore_linea));
					holder.text1 = new TextView(getApplicationContext());
					holder.text1.setLayoutParams(new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.FILL_PARENT));
					holder.text1.setTypeface(null, Typeface.BOLD);
					holder.text1.setTextColor(Color.BLACK);
					holder.text1.setGravity(Gravity.CENTER_VERTICAL);
					// text for S4 Meda
					if (linea == 4){
						holder.text2 = new TextView(getApplicationContext());
						holder.text2.setText("  /  Meda ");
						holder.text2.setLayoutParams(new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.WRAP_CONTENT,
								LinearLayout.LayoutParams.FILL_PARENT));
						holder.text2.setTypeface(null, Typeface.BOLD);
						holder.text2.setTextColor(Color.BLACK);
						holder.text2.setGravity(Gravity.CENTER_VERTICAL);
					}
					holder.layout_attr.setGravity(Gravity.CENTER_VERTICAL);
					holder.layout_attr.setPadding(2, 0, 0, 0);
					
					layout.setTag(holder);
				} else {
					holder = (ViewHolder)layout.getTag();
				}
				
				SElement lineElement = (SElement) listaMain.get(index);
				holder.image1.setImageBitmap(getImageForElement(lineElement, index));
//				if (linea==10){
//					if (index==0 || index==1 || index==2){
//						holder.image1.setBackgroundColor(Color.parseColor(MetroColors.s10t1));
//					} else {
//						holder.image1.setBackgroundColor(Color.parseColor(MetroColors.s10));
//					}
//				}
				holder.text1.setText(lineElement.getName());
				if (lineElement.getName().equals(searchLine)) {
					SpannableString underlineString = new SpannableString(holder.text1.getText());
					MetroUtils.setSearchStyle(underlineString);
					holder.text1.setText(underlineString);
				}
				((LinearLayout)layout).removeAllViews();
				((LinearLayout)layout).addView(holder.image1);
				((LinearLayout)layout).addView(holder.text1);
				holder.layout_attr.removeAllViews();
				for (MetroEnum attr : lineElement.getAttr()) {
					if (holder.attrs.get(attr)!=null) {
						holder.layout_attr.addView(holder.attrs.get(attr));
						if (linea==4 && index==0){
							holder.layout_attr.addView(holder.text2);
							holder.layout_attr.addView(holder.atR);
						}
					}
				}
				((LinearLayout)layout).addView(holder.layout_attr);
				
				return layout;
				
			}

			public long getItemId(int arg0) {
				return arg0;
			}

			public Object getItem(int arg0) {
				return listaMain.get(arg0);
			}

			public int getCount() {
				return listaMain.size();
			}

			private Bitmap getImageForElement(SElement element, int index) {
				if (index==0) {
					return btm_first;
				} else if (index==listaMain.size()-1) {
					return btm_last;
				} else if (element.getAttr().size()==0) {
					return btm_norm;
				} else {
					return btm_imp;
				}
			}

			public int getItemViewType(int arg0) {
				return 0;
			}

			public int getViewTypeCount() {
				return 1;
			}

			public boolean hasStableIds() {
				return true;
			}

			public boolean isEmpty() {
				return false;
			}

			public void registerDataSetObserver(DataSetObserver arg0) {
				
			}

			public void unregisterDataSetObserver(DataSetObserver arg0) {
				
			}

			public boolean areAllItemsEnabled() {
				return SData.ENABLE_LIST;
			}

			public boolean isEnabled(int arg0) {
				return SData.ENABLE_LIST;
			}
		};

		list.setAdapter(listAdapter);
	}
	
	private int convertNameToIndex() {
		String linea = (String)getIntent().getStringExtra("linea");
		linea = linea.substring(1);
		int index = 0;
		if (linea.length()>0) {
			index = Integer.parseInt(linea);
		}
		return index;
	}
}
