package com.sirowain.mm.route;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.sirowain.mm.R;
import com.sirowain.mm.data.ListElement;
import com.sirowain.mm.data.MetroData;
import com.sirowain.mm.data.MetroEnum;
import com.sirowain.mm.metro.TabMetro;
import com.sirowain.mm.passante.LineaSActivity;



public class BrowseAll extends Activity {

	static class ViewHolder {
		LinearLayout layout_row;
		TextView text;
		ImageView image;
	}
	
	Context context;
	ListView resultList;
	ArrayList<ListElement> arrayM = new ArrayList<ListElement>();
	Map<MetroEnum, Bitmap> bitmapMap;
	Intent intent_s;
	Intent intent_m;
	Bitmap m1;
	Bitmap m2;
	Bitmap m3;
	Bitmap s1;
	Bitmap s2;
	Bitmap s3;
	Bitmap s4;
	Bitmap s5;
	Bitmap s6;
	Bitmap s8;
	Bitmap s9;
	Bitmap s11;
	Bitmap s13;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (MetroData.ENABLE_ADS) {
			setContentView(R.layout.all_stations);
			// Look up the AdView as a resource and load a request.
		    AdView adView = (AdView)this.findViewById(R.id.ad);
		    
		    Map<String, Object> extras = new HashMap<String, Object>();
		    extras.put("color_bg", "#205fa4");
		    extras.put("color_bg_top", "#CCCCCC");
		    extras.put("color_text", "#FFFFFF");

		    AdRequest adRequest = new AdRequest();
		    adRequest.setExtras(extras);
		    adView.loadAd(adRequest);
		} else {
			setContentView(R.layout.all_stations_noads);
		}
		context = getApplicationContext();
		
		Resources res = getResources();
		prepareBitmaps(res);
		populateBitmapMap();
		intent_s = new Intent(context, LineaSActivity.class);
		intent_m = new Intent(context, TabMetro.class);
		
		resultList = (ListView)findViewById(R.id.list_stations);
		resultList.setAdapter(resultListAdapter);
		resultList.setDividerHeight(0);
		resultList.setCacheColorHint(Color.WHITE);
		resultList.setOnItemClickListener(clickListListener);
		resultList.setFastScrollEnabled(true);
		
		arrayM = MetroData.getAll();
		cleanList();
		Collections.sort(arrayM, comparator);
	}
	
	private void prepareBitmaps(Resources res) {
		m1 = BitmapFactory.decodeResource(res, R.drawable.m1);
		m2 = BitmapFactory.decodeResource(res, R.drawable.m2);
		m3 = BitmapFactory.decodeResource(res, R.drawable.m3);
		s1 = BitmapFactory.decodeResource(res, R.drawable.s1);
		s2 = BitmapFactory.decodeResource(res, R.drawable.s2);
		s3 = BitmapFactory.decodeResource(res, R.drawable.s3);
		s4 = BitmapFactory.decodeResource(res, R.drawable.s4);
		s5 = BitmapFactory.decodeResource(res, R.drawable.s5);
		s6 = BitmapFactory.decodeResource(res, R.drawable.s6);
		s8 = BitmapFactory.decodeResource(res, R.drawable.s8);
		s9 = BitmapFactory.decodeResource(res, R.drawable.s9);
		s11 = BitmapFactory.decodeResource(res, R.drawable.s11);
		s13 = BitmapFactory.decodeResource(res, R.drawable.s13);
	}
	
	private void populateBitmapMap() {
		bitmapMap = new HashMap<MetroEnum, Bitmap>();
		
		bitmapMap.put(MetroEnum.M1, m1);
		bitmapMap.put(MetroEnum.M2, m2);
		bitmapMap.put(MetroEnum.M3, m3);
		bitmapMap.put(MetroEnum.S1, s1);
		bitmapMap.put(MetroEnum.S2, s2);
		bitmapMap.put(MetroEnum.S3, s3);
		bitmapMap.put(MetroEnum.S4, s4);
		bitmapMap.put(MetroEnum.S5, s5);
		bitmapMap.put(MetroEnum.S6, s6);
		bitmapMap.put(MetroEnum.S8, s8);
		bitmapMap.put(MetroEnum.S9, s9);
		bitmapMap.put(MetroEnum.S11,s11);
		bitmapMap.put(MetroEnum.S13,s13);
		
	}
	
	private void cleanList() {
		for (Iterator<ListElement> iterator = arrayM.iterator(); iterator.hasNext();) {
			ListElement elem = (ListElement) iterator.next();
			if (elem.getName().length()<=0) {
				iterator.remove();
			}
		}
	}
	
	private Comparator<ListElement> comparator = new Comparator<ListElement>() {

		public int compare(ListElement object1, ListElement object2) {
			return object1.getName().compareTo(object2.getName());
		}
	};
	
	private OnItemClickListener clickListListener = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			ListElement selected = (ListElement) resultListAdapter.getItem(position);
			
			String text = selected.getName();
			if (text.contains("Porta")) {
				text = text.replace("Porta","P.ta");
			}
			
			int defaultTab = 0;
			if (selected.getProv().equals(MetroEnum.M1)) {
				defaultTab = 0;
			} else if (selected.getProv().equals(MetroEnum.M2)) {
				defaultTab = 1;
			} else if (selected.getProv().equals(MetroEnum.M3)) {
				defaultTab = 2;
			}
			
			if (selected.getProv().getName().startsWith("S")) {
				intent_s.putExtra("linea", selected.getProv().getName());
				intent_s.putExtra(MetroData.APP_PACKAGE+".searchLine", text);
				startActivity(intent_s);
			} else if (selected.getProv().getName().startsWith("M")) {
				intent_m.putExtra(MetroData.APP_PACKAGE+".defaultTab", defaultTab);
				intent_m.putExtra(MetroData.APP_PACKAGE+".searchLine", text);
				startActivity(intent_m);
			}
		}
	};

	MyAdapter resultListAdapter = new MyAdapter() {
		
		public View getView(int position, View layout_row, ViewGroup parent) {
			ViewHolder holder;
			if (layout_row==null) {
				layout_row = new LinearLayout(getApplicationContext());
				layout_row.setLayoutParams(new AbsListView.LayoutParams(
						LinearLayout.LayoutParams.FILL_PARENT,
						LinearLayout.LayoutParams.FILL_PARENT));
				layout_row.setHorizontalFadingEdgeEnabled(false);
				((LinearLayout)layout_row).setBaselineAligned(false);
				holder = new ViewHolder();
				holder.layout_row = new LinearLayout(getApplicationContext());
				
				holder.text = new TextView(context);
				holder.image = new ImageView(context);
				
				holder.layout_row.setLayoutParams(new AbsListView.LayoutParams(
						LinearLayout.LayoutParams.FILL_PARENT,
						LinearLayout.LayoutParams.FILL_PARENT));
	
				holder.text.setLayoutParams(new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.FILL_PARENT,
						LinearLayout.LayoutParams.FILL_PARENT));
				holder.text.setPadding(MetroData.LIST_L_PAD,
						MetroData.LIST_T_PAD, MetroData.LIST_R_PAD,
						MetroData.LIST_B_PAD);
				holder.text.setTypeface(null, Typeface.BOLD);
				holder.text.setTextColor(Color.BLACK);
				holder.text.setGravity(Gravity.CENTER_VERTICAL);
				layout_row.setTag(holder);
			} else {
				holder = (ViewHolder)layout_row.getTag();
			}
			
			ListElement result = (ListElement) arrayM.get(position);
			holder.image.setImageBitmap(bitmapMap.get(result.getProv()));
			if (result!=null) {
				holder.text.setText(result.getName());
			}
			((LinearLayout)layout_row).removeAllViews();
			holder.layout_row.removeAllViews();
			((LinearLayout) holder.layout_row).addView(holder.image, 0);
			((LinearLayout) holder.layout_row).addView(holder.text, 1);
			
			((LinearLayout)layout_row).addView(holder.layout_row);
			
			return layout_row;
		}
		
		public long getItemId(int position) {
			return position;
		}
		
		public Object getItem(int position) {
			return arrayM.get(position);
		}
		
		public int getCount() {
			return arrayM.size();
		}
		
		public int getPositionForSection(int section) {
			if (section==0) {
				return 0;
			} else {
				return 1;
			}
		}

		public int getSectionForPosition(int position) {
			if (position>20) {
				return 1;
			}else {
				return 0;
			}
		}
		
		public Object[] getSections() {
			
			return new String[] {"A,B"};
		}
	};
	
}

class MyAdapter extends BaseAdapter implements SectionIndexer {

	public int getPositionForSection(int section) {
		return 0;
	}

	public int getSectionForPosition(int position) {
		return 0;
	}

	public Object[] getSections() {
		
		return null;
	}

	public int getCount() {
		return 0;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		return null;
	}
	
}
