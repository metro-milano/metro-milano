package com.sirowain.mm.route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView.Validator;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.sirowain.mm.R;
import com.sirowain.mm.data.ListElement;
import com.sirowain.mm.data.MetroData;
import com.sirowain.mm.data.MetroEnum;
import com.sirowain.mm.metro.TabMetro;
import com.sirowain.mm.passante.LineaSActivity;

public class Route extends Activity{
	AutoCompleteTextView text_search;
	ArrayList<String> fermate ;
	TextView textview;
	TextView text_header;
	Map<String,ArrayList<MetroEnum>> struct;
	ListView resultList;
	ArrayList<MetroEnum> results = new ArrayList<MetroEnum>();
	Context context;
	Map<MetroEnum, Bitmap> bitmapMap;
	Intent intent_s;
	Intent intent_m;
	InputMethodManager inputManager;
	Bitmap m1;
	Bitmap m2;
	Bitmap m3;
	Bitmap s1;
	Bitmap s2;
	Bitmap s3;
	Bitmap s4;
	Bitmap s5;
	Bitmap s6;
	Bitmap s8;
	Bitmap s9;
	Bitmap s11;
	Bitmap s13;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (MetroData.ENABLE_ADS) {
			setContentView(R.layout.search);
			// Look up the AdView as a resource and load a request.
		    AdView adView = (AdView)this.findViewById(R.id.ad);
		    
		    Map<String, Object> extras = new HashMap<String, Object>();
		    extras.put("color_bg", "#205fa4");
		    extras.put("color_bg_top", "#CCCCCC");
		    extras.put("color_text", "#FFFFFF");

		    AdRequest adRequest = new AdRequest();
		    adRequest.setExtras(extras);
		    adView.loadAd(adRequest);
		} else {
			setContentView(R.layout.search_noads);
		}
		context = getApplicationContext();
		
		Resources res = getResources();
		prepareBitmaps(res);
		populateBitmapMap();
		intent_s = new Intent(context, LineaSActivity.class);
		intent_m = new Intent(context, TabMetro.class);
		inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		
		text_search = (AutoCompleteTextView) findViewById(R.id.route_to);
		text_header = (TextView) findViewById(R.id.list_header);
		
		resultList = (ListView)findViewById(R.id.list_results);
		resultList.setAdapter(resultListAdapter);
		resultList.setDividerHeight(0);
		resultList.setCacheColorHint(Color.WHITE);
		resultList.setOnItemClickListener(clickListListener);
		
		ArrayList<ListElement> arrayM = MetroData.getAll();
		fermate = getAllNames(arrayM);

		textview = (TextView) findViewById(R.id.TextView01);
		struct = getSearchStructure(arrayM);
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, fermate);
		text_search.setAdapter(adapter);
		text_search.setValidator(validator);
		text_search.setOnItemClickListener(clickSearchListener);
	}
	
	private void prepareBitmaps(Resources res) {
		m1 = BitmapFactory.decodeResource(res, R.drawable.m1);
		m2 = BitmapFactory.decodeResource(res, R.drawable.m2);
		m3 = BitmapFactory.decodeResource(res, R.drawable.m3);
		s1 = BitmapFactory.decodeResource(res, R.drawable.s1);
		s2 = BitmapFactory.decodeResource(res, R.drawable.s2);
		s3 = BitmapFactory.decodeResource(res, R.drawable.s3);
		s4 = BitmapFactory.decodeResource(res, R.drawable.s4);
		s5 = BitmapFactory.decodeResource(res, R.drawable.s5);
		s6 = BitmapFactory.decodeResource(res, R.drawable.s6);
		s8 = BitmapFactory.decodeResource(res, R.drawable.s8);
		s9 = BitmapFactory.decodeResource(res, R.drawable.s9);
		s11 = BitmapFactory.decodeResource(res, R.drawable.s11);
		s13 = BitmapFactory.decodeResource(res, R.drawable.s13);
	}
	
	private void populateBitmapMap() {
		bitmapMap = new HashMap<MetroEnum, Bitmap>();
		
		bitmapMap.put(MetroEnum.M1, m1);
		bitmapMap.put(MetroEnum.M2, m2);
		bitmapMap.put(MetroEnum.M3, m3);
		bitmapMap.put(MetroEnum.S1, s1);
		bitmapMap.put(MetroEnum.S2, s2);
		bitmapMap.put(MetroEnum.S3, s3);
		bitmapMap.put(MetroEnum.S4, s4);
		bitmapMap.put(MetroEnum.S5, s5);
		bitmapMap.put(MetroEnum.S6, s6);
		bitmapMap.put(MetroEnum.S8, s8);
		bitmapMap.put(MetroEnum.S9, s9);
		bitmapMap.put(MetroEnum.S11,s11);
		bitmapMap.put(MetroEnum.S13,s13);
		
	}
	
	private ArrayList<String> getAllNames(ArrayList<ListElement> arrayM) {
		ArrayList<String> names = new ArrayList<String>();
		
		// Loads stations names
		for (ListElement metroElement : arrayM) {
			String name = metroElement.getName();
			if (name.contains("P.ta")) {
				name = name.replace("P.ta","Porta");
			}
			if (!names.contains(name)) {
				names.add(name);
			}
		}
		return names;
	}
	
	private Map<String,ArrayList<MetroEnum>> getSearchStructure(ArrayList<ListElement> arrayM) {
		Map<String,ArrayList<MetroEnum>> struct = new HashMap<String, ArrayList<MetroEnum>>();
		
		// Loads stations names
		for (ListElement metroElement : arrayM) {
			String name = metroElement.getName();
			if (!struct.containsKey(name) && name.length()>0) {
				ArrayList<MetroEnum> provs = new ArrayList<MetroEnum>();
				provs.add(metroElement.getProv());
				struct.put(name,provs);
			} else if (name.length()>0){
				ArrayList<MetroEnum> provs = struct.get(name);
				if (metroElement.getProv()!=null) {
					provs.add(metroElement.getProv());
				}
			}
		}
		return struct;
	}
	
	private OnItemClickListener clickSearchListener = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			String text = text_search.getText().toString();
			if (text.contains("Porta")) {
				text = text.replace("Porta","P.ta");
			}
			results = struct.get(text);
			text_header.setText(R.string.lines_at_station);
			text_header.append(" " + text + ":");
			text_header.setVisibility(View.VISIBLE);
			resultListAdapter.notifyDataSetChanged();
			inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS); 
		}
	};
	
	private OnItemClickListener clickListListener = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			MetroEnum selected = (MetroEnum) resultListAdapter.getItem(position);
			
			String text = text_search.getText().toString();
			if (text.contains("Porta")) {
				text = text.replace("Porta","P.ta");
			}
			
			int defaultTab = 0;
			if (selected.equals(MetroEnum.M1)) {
				defaultTab = 0;
			} else if (selected.equals(MetroEnum.M2)) {
				defaultTab = 1;
			} else if (selected.equals(MetroEnum.M3)) {
				defaultTab = 2;
			}
			
			if (selected.getName().startsWith("S")) {
				intent_s.putExtra("linea", selected.getName());
				intent_s.putExtra(MetroData.APP_PACKAGE+".searchLine", text);
				startActivity(intent_s);
			} else if (selected.getName().startsWith("M")) {
				intent_m.putExtra(MetroData.APP_PACKAGE+".defaultTab", defaultTab);
				intent_m.putExtra(MetroData.APP_PACKAGE+".searchLine", text);
				startActivity(intent_m);
			}
		}
	};
	
	BaseAdapter resultListAdapter = new BaseAdapter() {
		
		public View getView(int position, View layout_row, ViewGroup parent) {
			layout_row = new LinearLayout(getApplicationContext());
			MetroEnum result = (MetroEnum) results.get(position);
			TextView textview = new TextView(context);
			ImageView image = new ImageView(context);
			image.setImageBitmap(bitmapMap.get(result));
			
			layout_row.setLayoutParams(new AbsListView.LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT,
					LinearLayout.LayoutParams.FILL_PARENT));

			textview.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT,
					LinearLayout.LayoutParams.FILL_PARENT));
			textview.setPadding(MetroData.LIST_L_PAD,
					MetroData.LIST_T_PAD, MetroData.LIST_R_PAD,
					MetroData.LIST_B_PAD);
			textview.setTypeface(null, Typeface.BOLD);
			textview.setTextColor(Color.BLACK);
			textview.setGravity(Gravity.CENTER_VERTICAL);
			
			if (result!=null) {
				textview.setText(result.getDescr());
			}
			((LinearLayout) layout_row).addView(image, 0);
			((LinearLayout) layout_row).addView(textview, 1);
			return layout_row;
		}
		
		public long getItemId(int position) {
			return position;
		}
		
		public Object getItem(int position) {
			return results.get(position);
		}
		
		public int getCount() {
			return results.size();
		}
		
	};
	
	Validator validator = new Validator() {
		
		public boolean isValid(CharSequence text) {
			return fermate.contains(text.toString());
		}
		
		public CharSequence fixText(CharSequence invalidText) {
			return null;
		}
	};

}
